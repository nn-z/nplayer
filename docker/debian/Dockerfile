FROM debian

# prepare mxe env
RUN apt-get -y update &&\
	apt-get -y install autoconf automake autopoint bash bison bzip2 flex g++ g++-multilib gettext git gperf intltool libc6-dev-i386 libgdk-pixbuf2.0-dev libltdl-dev libssl-dev libtool-bin libxml-parser-perl make openssl p7zip-full patch perl pkg-config python ruby scons sed unzip wget xz-utils git wget &&\
	cd /opt &&\
	git clone https://github.com/mxe/mxe.git &&\
	cd /opt/mxe &&\
	make -j$(nproc) qtbase qtwinextras qtgraphicaleffects qtquickcontrols2 qtquickcontrols &&\
	export PATH=/opt/mxe/usr/bin:$PATH

# download mpv
RUN cd /opt &&\
	mkdir -p mpv/tmp mpv/lib mpv/lib64 mpv/include/mpv &&\
	cd mpv/tmp &&\
	wget https://sourceforge.net/projects/mpv-player-windows/files/libmpv/mpv-dev-x86_64-20180527-git-fae9d77.7z/download &&\
	mv download mpv64.7z &&\
	7za x mpv64.7z &&\
	mv include/* ../include/mpv/ &&\
	mv *.dll* ../lib64 &&\
	rm -rf * &&\
	wget https://sourceforge.net/projects/mpv-player-windows/files/libmpv/mpv-dev-i686-20180527-git-fae9d77.7z/download &&\
        mv download mpv32.7z &&\
        7za x mpv32.7z &&\
	mv *.dll* ../lib &&\
	cd .. &&\
	rm -rf tmp/

# download openssl
RUN cd /opt &&\
	mkdir -p openssl/tmp openssl/lib openssl/lib64 &&\
	cd openssl/tmp &&\
	wget https://indy.fulgan.com/SSL/openssl-1.0.2o-i386-win32.zip &&\
	unzip openssl-1.0.2o-i386-win32.zip &&\
	mv *.dll* ../lib &&\
	rm * &&\
	wget https://indy.fulgan.com/SSL/openssl-1.0.2o-x64_86-win64.zip &&\
	unzip openssl-1.0.2o-x64_86-win64.zip &&\
	mv *.dll* ../lib64 &&\
	cd .. &&\
	rm -rf tmp/

# download youtube-dl
RUN cd /opt &&\
	mkdir ytdl &&\
	cd ytdl &&\
	wget https://yt-dl.org/downloads/2018.05.30/youtube-dl.exe
