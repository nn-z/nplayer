import QtQuick 2.0
import QtQuick.Controls 2.3

import NPlayer 1.0

Item {
    id: root
    enabled: mainWindow.actionsEnabled

    property alias primary: primary
    property alias secondary: secondary

    // name of the action to lookup from action handler
    property string action

    property bool hint: false
    // custom hint text, if null then Action.text is used
    property string hintText

    property string text: ""
    property bool checkable: false
    property bool checked: false

    signal triggered

    Connections {
        target: ActionHandler
        onMapChanged: updateShortcuts()
    }

    function updateShortcuts() {
        primary.shortcut = ActionHandler.shortcut(ActionHandler.Primary, action)
        secondary.shortcut = ActionHandler.shortcut(ActionHandler.Secondary, action)
    }

    Action {
        id: primary
        enabled: root.enabled
        text: root.text
        checkable: root.checkable
        checked: root.checked
        onTriggered: {
            root.triggered()

             if (hint) {
                if (hintText && hintText.length > 0) {
                    onScreenHint.show(hintText)
                } else {
                    onScreenHint.show(text)
                }
            }
        }
    }

    Action {
        id: secondary
        enabled: root.enabled
        onTriggered: primary.trigger()
    }
}
