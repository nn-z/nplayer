import QtQuick 2.0
import QtQuick.Window 2.10

import NPlayer 1.0

MpvObject {
    id: renderer
    objectName: "renderer"
    anchors.fill: parent

    onFileChanged: {
        if (NSettings.autoVideoResize && (mainWindow.visibility === Window.Windowed)) {
            mainWindow.width = renderer.dimensions.x
            mainWindow.height = renderer.dimensions.y
        }
    }

    Timer {
        id: rendererMouseHideTimer
        interval: NSettings.cursorHideTime
        running: false
        repeat: false
        onTriggered: rendererMouseArea.cursorShape = Qt.BlankCursor
    }

    MouseArea {
        id: rendererMouseArea
        anchors.fill: parent
        acceptedButtons: Qt.RightButton | Qt.LeftButton
        hoverEnabled: true
        onClicked: {
            if (mouse.button === Qt.RightButton)
                contextMenu.menu.popup()
        }
        onPressAndHold: {
            if (mouse.source === Qt.MouseEventNotSynthesized)
                contextMenu.menu.popup()
        }
        onDoubleClicked: {
            if (mouse.button === Qt.LeftButton)
                mainWindow.visibility = (mainWindow.visibility === Window.FullScreen) ? Window.Windowed : Window.FullScreen
        }
        onPositionChanged: {
            onScreenControls.show()
            if (NSettings.cursorHideTime > 0) {
                cursorShape = Qt.ArrowCursor
                rendererMouseHideTimer.restart()
            }
        }
    }
}
