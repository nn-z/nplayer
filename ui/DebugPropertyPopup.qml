import QtQuick 2.0
import QtQuick.Controls 2.2

Popup {
    id: popupDebugProperty
    x: ((mainWindow.width + width) / 2) - width
    y: ((mainWindow.height + height) / 2) - height
    width: 300
    height: 50
    modal: true
    focus: true
    closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent

    ComboBox {
        focus: true
        anchors.fill: parent
        editable: true
        onAccepted: {
            renderer.dumpProperty(currentText)
            popupDebugProperty.close()
        }

        model: ListModel {
            ListElement { key: "display-sync-active" }
            ListElement { key: "filename" }
            ListElement { key: "file-size" }
            ListElement { key: "estimated-frame-count" }
            ListElement { key: "estimated-frame-number" }
            ListElement { key: "path" }
            ListElement { key: "media-title" }
            ListElement { key: "file-format" }
            ListElement { key: "duration" }
            ListElement { key: "avsync" }
            ListElement { key: "decoder-frame-drop-count" }
            ListElement { key: "frame-drop-count" }
            ListElement { key: "mistimed-frame-count" }
            ListElement { key: "vsync-ratio" }
            ListElement { key: "percent-pos" }
            ListElement { key: "time-pos" }
            ListElement { key: "time-remaining" }
            ListElement { key: "playtime-remaining" }
            ListElement { key: "chapter" }
            ListElement { key: "chapters" }
            ListElement { key: "cache" }
            ListElement { key: "cache-size" }
            ListElement { key: "cache-free" }
            ListElement { key: "cache-used" }
            ListElement { key: "cache-speed" }
            ListElement { key: "cache-idle" }
            ListElement { key: "paused-for-cache" }
            ListElement { key: "cache-buffering-state" }
            ListElement { key: "eof-reached" }
            ListElement { key: "seeking" }
            ListElement { key: "ao-volume" }
            ListElement { key: "ao-mute" }
            ListElement { key: "audio-codec" }
            ListElement { key: "audio-codec-name" }
            ListElement { key: "hwdec" }
            ListElement { key: "hwdec-current" }
            ListElement { key: "hwdec-interop" }
            ListElement { key: "video-format" }
            ListElement { key: "video-codec" }
            ListElement { key: "width" }
            ListElement { key: "height" }
            ListElement { key: "dwidth" }
            ListElement { key: "dheight" }
            ListElement { key: "container-fps" }
            ListElement { key: "estimated-vf-fps" }
            ListElement { key: "window-scale" }
            ListElement { key: "display-fps" }
            ListElement { key: "estimated-display-fps" }
            ListElement { key: "vsync-jitter" }
            ListElement { key: "video-aspect" }
            ListElement { key: "current-vo" }
            ListElement { key: "current-ao" }
            ListElement { key: "mpv-version" }
            ListElement { key: "ffmpeg-version" }
            ListElement { key: "vid" }
            ListElement { key: "aid" }
            ListElement { key: "sid" }
        }
    }
}
