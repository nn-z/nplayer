import QtQuick 2.0
import QtQuick.Window 2.10
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3
import QtQuick.Dialogs 1.3

import NPlayer 1.0
import "config"

Window {
    id: configWindow
    width: 700
    height: 500
    flags: Qt.Window | Qt.WindowTitleHint

    signal save
    signal reset

    Component.onCompleted: reset()
    onVisibleChanged: view.currentIndex = 0

    ColumnLayout {
        anchors.fill: parent
        anchors.margins: 10

        RowLayout {
            Rectangle {
                Layout.preferredWidth: 100
                Layout.fillHeight: true

                border.color: "lightgray"
                border.width: 1

                ListView {
                    id: view
                    anchors.fill: parent

                    ScrollBar.vertical: ScrollBar {}

                    highlightMoveDuration : 0
                    highlightResizeDuration: 0
                    highlight: Rectangle {
                        color: "lightgray"
                    }

                    model: ListModel {
                        id: model
                        ListElement { tab: "General" }
                        ListElement { tab: "UI" }
                        ListElement { tab: "Video" }
                        ListElement { tab: "Audio" }
                        ListElement { tab: "Subtitles" }
                        ListElement { tab: "Playlist" }
                        ListElement { tab: "History" }
                        ListElement { tab: "Menu" }
                        ListElement { tab: "Log" }
                        ListElement { tab: "Shortcuts" }
                    }

                    Component.onCompleted: {
                        if (Qt.platform.os === "windows") {
                            model.append({'tab': "Updates"})
                        }
                    }

                    delegate: Item {
                        height: 32
                        width: view.width

                        Text {
                            id: tabText
                            anchors.fill: parent
                            anchors.margins: 5
                            verticalAlignment: Text.AlignVCenter
                            text: tab
                        }

                        MouseArea {
                            anchors.fill: parent
                            acceptedButtons: Qt.LeftButton
                            onClicked: {
                                view.currentIndex = index
                            }
                        }
                    }
                }
            }

            StackLayout {
                currentIndex: view.currentIndex
                clip: true
                ScrollView {
                    id: generalTab
                    ScrollBar.vertical.policy: ScrollBar.AlwaysOn

                    ColumnLayout {
                        GroupBox {
                            title: qsTr("Cursor")
                            NSpinBox {
                                id: cursorHideTime
                                label: qsTr("Hide time (ms)")
                                from: 0
                                to: 60000
                                Connections {
                                    target: configWindow
                                    onReset: cursorHideTime.value = NSettings.cursorHideTime
                                    onSave: NSettings.cursorHideTime = cursorHideTime.value
                                }
                            }
                        }

                        GroupBox {
                            title: qsTr("On Screen Controls")
                            NSpinBox {
                                id: onScreenControlsHideTime
                                label: qsTr("Hide time (ms)")
                                from: 0
                                to: 60000
                                Connections {
                                    target: configWindow
                                    onReset: onScreenControlsHideTime.value = NSettings.onScreenControlsHideTime
                                    onSave: NSettings.onScreenControlsHideTime = onScreenControlsHideTime.value
                                }
                            }
                        }

                        GroupBox {
                            title: qsTr("On Screen Seek Bar")
                            ColumnLayout {
                                NSpinBox {
                                    id: onScreenSeekBarHideTime
                                    label: qsTr("Hide time (ms)")
                                    from: 0
                                    to: 60000
                                    Connections {
                                        target: configWindow
                                        onReset: onScreenSeekBarHideTime.value = NSettings.onScreenControlsHideTime
                                        onSave: NSettings.onScreenControlsHideTime = onScreenSeekBarHideTime.value
                                    }
                                }

                                DoubleSpinBox {
                                    id: onScreenSeekBarOpacity
                                    label: qsTr("Opacity")
                                    Connections {
                                        target: configWindow
                                        onReset: onScreenSeekBarOpacity.value = 100 * NSettings.onScreenSeekBarOpacity
                                        onSave: NSettings.onScreenSeekBarOpacity = onScreenSeekBarOpacity.value / 100
                                    }
                                }
                            }
                        }

                        GroupBox {
                            title: qsTr("On Screen Hint")
                            NSpinBox {
                                id: onScreenHintHideTime
                                label: qsTr("Hide time (ms)")
                                from: 0
                                to: 60000
                                Connections {
                                    target: configWindow
                                    onReset: onScreenHintHideTime.value = NSettings.onScreenHintHideTime
                                    onSave: NSettings.onScreenHintHideTime = onScreenHintHideTime.value
                                }
                            }
                        }
                    }
                }
                ColumnLayout {
                    id: uiTab

                    NCheckBox {
                        id: nativeFileDialog
                        text: qsTr("Use native file dialog")
                        Connections {
                            target: configWindow
                            onReset: nativeFileDialog.checked = NSettings.uiNativeFileDialog
                            onSave: NSettings.uiNativeFileDialog = nativeFileDialog.checked
                        }
                    }

                    GroupBox {
                        title: qsTr("UI font")
                        Layout.fillWidth: true
                        Layout.preferredHeight: 310

                        ColumnLayout {
                            anchors.fill: parent
                            NCheckBox {
                                id: customFont
                                text: qsTr("Customize font")
                                Connections {
                                    target: configWindow
                                    onReset: customFont.checked = NSettings.uiFontCustom
                                    onSave: NSettings.uiFontCustom = customFont.checked
                                }
                            }

                            FontViewer {
                                id: uiFont
                                Layout.fillWidth: true
                                Layout.fillHeight: true
                                Connections {
                                    target: configWindow
                                    onReset: {
                                        var sff = NSettings.uiFontFamily
                                        var ff = sff.length > 0 ? sff : "Arial"
                                        uiFont.setFontFamily(ff)
                                        uiFont.setFontSize(NSettings.uiFontSize)
                                    }
                                    onSave: {
                                        if (customFont.enabled) {
                                            NSettings.uiFontFamily = uiFont.fontFamily
                                            NSettings.uiFontSize = uiFont.fontSize
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                ColumnLayout {
                    id: videoTab

                    CheckBox {
                        id: hwDecoding
                        text: qsTr("HW decoding")
                        ToolTip.text:  qsTr("Enables hardware decoding, can cause tearing in 60fps videos")
                        ToolTip.visible: hovered
                        ToolTip.delay: 500
                        Connections {
                            target: configWindow
                            onReset: hwDecoding.checked = NSettings.hwDecoding
                            onSave: NSettings.hwDecoding = hwDecoding.checked
                        }
                    }

                    CheckBox {
                        id: autoVideoResize
                        text: qsTr("Auto resize window")
                        ToolTip.text:  qsTr("Allows resizing windows when loading video, only works in windowed mode")
                        ToolTip.visible: hovered
                        ToolTip.delay: 500
                        Connections {
                            target: configWindow
                            onReset: autoVideoResize.checked = NSettings.autoVideoResize
                            onSave: NSettings.autoVideoResize = autoVideoResize.checked
                        }
                    }

                    GroupBox {
                        title: qsTr("Seek")
                        ColumnLayout {
                            DoubleSpinBox {
                                id: seekShort
                                label: qsTr("Short seek (s)")
                                to: 3600
                                Connections {
                                    target: configWindow
                                    onReset: seekShort.value = 100 * NSettings.seekShort
                                    onSave: NSettings.seekShort = seekShort.value / 100
                                }
                            }

                            DoubleSpinBox {
                                id: seekMedium
                                label: qsTr("Medium seek (s)")
                                to: 3600
                                Connections {
                                    target: configWindow
                                    onReset: seekMedium.value = 100 * NSettings.seekMedium
                                    onSave: NSettings.seekMedium = seekMedium.value / 100
                                }
                            }

                            DoubleSpinBox {
                                id: seekLong
                                label: qsTr("Long seek (s)")
                                to: 3600
                                Connections {
                                    target: configWindow
                                    onReset: seekLong.value = 100 * NSettings.seekLong
                                    onSave: NSettings.seekLong = seekLong.value / 100
                                }
                            }
                        }
                    }
                }
                ColumnLayout {
                    id: audioTab

                    NTextField {
                        id: audioDefaultTrack
                        label: qsTr("Default audio track")
                        placeholderText: qsTr("eng")
                        Connections {
                            target: configWindow
                            onReset: audioDefaultTrack.text = NSettings.audioDefaultTrack
                            onSave: NSettings.audioDefaultTrack = audioDefaultTrack.text
                        }
                    }

                    NSpinBox {
                        id: volumeStep
                        label: qsTr("Volume step size (%)")
                        from: 0
                        to: 100
                        Connections {
                            target: configWindow
                            onReset: volumeStep.value = NSettings.volumeStep
                            onSave: NSettings.volumeStep = volumeStep.value
                        }
                    }
                }
                ColumnLayout {
                    id: subtitlesTab

                    NTextField {
                        id: subDefaultTrack
                        label: qsTr("Default subtitle track")
                        placeholderText: qsTr("eng")
                        Connections {
                            target: configWindow
                            onReset: subDefaultTrack.text = NSettings.subDefaultTrack
                            onSave: NSettings.subDefaultTrack = subDefaultTrack.text
                        }
                    }

                    NComboBox {
                        id: subAutoLoad
                        label: qsTr("Auto load")
                        ToolTip.text: qsTr("None: Don't automatically load external subtitle files\nExact: Load the media filename with subtitle file extension\nFuzzy: Load all subs containing media filename")
                        model: ListModel {
                            id: subAutoLoadModel
                            ListElement { text: qsTr("None") }
                            ListElement { text: qsTr("Exact") }
                            ListElement { text: qsTr("Fuzzy") }
                        }
                        Connections {
                            target: configWindow
                            onReset: subAutoLoad.currentIndex = NSettings.subAutoLoad
                            onSave: NSettings.subAutoLoad = subAutoLoad.currentIndex
                        }
                    }
                }
                ColumnLayout {
                    id: playlistTab

                    NCheckBox {
                        id: playlistPopulateFromSubdirectories
                        text: qsTr("Populate playlist from subdirectories")
                        ToolTip.text:  qsTr("Allows iterating subdirectories of item/folder")
                        Connections {
                            target: configWindow
                            onReset: playlistPopulateFromSubdirectories.checked = NSettings.playlistPopulateFromSubdirectories
                            onSave: NSettings.playlistPopulateFromSubdirectories = playlistPopulateFromSubdirectories.checked
                        }
                    }

                    NCheckBox {
                        id: playlistRepopulateOnDrag
                        text: qsTr("Repopulate playlist on files dragged")
                        ToolTip.text:  qsTr("Allows loading all files from folder where dragged file is, also erases current playlist")
                        Connections {
                            target: configWindow
                            onReset: playlistRepopulateOnDrag.checked = NSettings.playlistRepopulateOnDrag
                            onSave: NSettings.playlistRepopulateOnDrag = playlistRepopulateOnDrag.checked
                        }
                    }

                    NCheckBox {
                        id: playlistFileSystemWatch
                        text: qsTr("Watch playlist")
                        ToolTip.text:  qsTr("Allows automatic adding/removing files from loaded directory\nDoesn't work with playlistPopulateFromSubdirectories option")
                        Connections {
                            target: configWindow
                            onReset: playlistFileSystemWatch.checked = NSettings.playlistFileSystemWatch
                            onSave: NSettings.playlistFileSystemWatch = playlistFileSystemWatch.checked
                        }
                    }
                }
                ColumnLayout {
                    id: historyTab

                    CheckBox {
                        id: historyEnabled
                        text: qsTr("Enabled")
                        Connections {
                            target: configWindow
                            onReset: historyEnabled.checked = NSettings.historyEnabled
                            onSave: NSettings.historyEnabled = historyEnabled.checked
                        }
                    }
                    NSpinBox {
                        id: historyMaxSize
                        label: qsTr("Max size")
                        from: -1
                        to: 10000
                        ToolTip.text:  qsTr("Max history size, -1 for unlimited")
                        Connections {
                            target: configWindow
                            onReset: historyMaxSize.value = NSettings.historyMaxSize
                            onSave: NSettings.historyMaxSize = historyMaxSize.value
                        }
                    }
                }
                ColumnLayout {
                    id: menuTab

                    CheckBox {
                        id: menuItemShowBothShortcuts
                        text: qsTr("Show both shortcuts")
                        Connections {
                            target: configWindow
                            onReset: menuItemShowBothShortcuts.checked = NSettings.menuItemShowBothShortcuts
                            onSave: NSettings.menuItemShowBothShortcuts = menuItemShowBothShortcuts.checked
                        }
                    }
                }
                ColumnLayout {
                    id: logTab

                    NComboBox {
                        id: logType
                        label: qsTr("Log type")
                        model: ListModel {
                            id: logTypeModel
                            ListElement { text: qsTr("None") }
                            ListElement { text: qsTr("Console") }
                        }
                        Connections {
                            target: configWindow
                            onReset: logType.currentIndex = NSettings.logType
                            onSave: NSettings.logType = logType.currentIndex
                        }
                    }
                    NComboBox {
                        id: mpvMsgLevel
                        enabled: logType.currentIndex > 0
                        label: qsTr("mpv message level")
                        model: ListModel {
                            id: mpvMsgLevelModel
                            ListElement { text: qsTr("None") }
                            ListElement { text: qsTr("Fatal") }
                            ListElement { text: qsTr("Error") }
                            ListElement { text: qsTr("Warn") }
                            ListElement { text: qsTr("Info") }
                            ListElement { text: qsTr("Status") }
                            ListElement { text: qsTr("Verbose") }
                            ListElement { text: qsTr("Debug") }
                            ListElement { text: qsTr("Trace") }
                        }
                        Connections {
                            target: configWindow
                            onReset: mpvMsgLevel.currentIndex = NSettings.mpvMsgLevel
                            onSave: NSettings.mpvMsgLevel = mpvMsgLevel.currentIndex
                        }
                    }
                }
                ScrollView {
                    id: shortcutsTab
                    ScrollBar.vertical.policy: ScrollBar.AlwaysOn

                    ColumnLayout {
                        RowLayout {
                            Text {
                                Layout.preferredWidth: 200
                                text: qsTr("Action")
                                font.underline: true
                            }
                            Text {
                                Layout.preferredWidth: 125
                                text: qsTr("Primary")
                                font.underline: true
                            }
                            Text {
                                Layout.preferredWidth: 125
                                text: qsTr("Secondary")
                                font.underline: true
                            }
                        }

                        Repeater {
                            model: ActionHandler.num()
                            RowLayout {
                                signal validateShortcut(TextField source, string shortcut)

                                Text {
                                    Layout.preferredWidth: 200
                                    text: ActionHandler.actionByIndex(index)
                                }

                                TextField {
                                    id: primaryShortcut
                                    Layout.preferredWidth: 125
                                    readOnly: true
                                    Keys.onPressed: {
                                        if (event.key === Qt.Key_Backspace || event.key === Qt.Key_Delete) {
                                            text = ""
                                        } else if (event.text.length > 0) {
                                            var ctrl = (event.modifiers & Qt.ControlModifier) ? "Ctrl+" : ""
                                            var alt = (event.modifiers & Qt.AltModifier) ? "Alt+" : ""
                                            var shift = (event.modifiers & Qt.ShiftModifier) ? "Shift+" : ""
                                            var meta = (event.modifiers & Qt.MetaModifier) ? "Meta+" : ""
                                            text = ctrl + alt + shift + meta + renderer.keyToAscii(event.key)
                                        }
                                        validateShortcut(this, text)
                                    }
                                }

                                TextField {
                                    id: secondaryShortcut
                                    Layout.preferredWidth: 125
                                    readOnly: true
                                    Keys.onPressed: {
                                        if (event.key === Qt.Key_Backspace || event.key === Qt.Key_Delete) {
                                            text = ""
                                        } else if (event.text.length > 0) {
                                            var ctrl = (event.modifiers & Qt.ControlModifier) ? "Ctrl+" : ""
                                            var alt = (event.modifiers & Qt.AltModifier) ? "Alt+" : ""
                                            var shift = (event.modifiers & Qt.ShiftModifier) ? "Shift+" : ""
                                            var meta = (event.modifiers & Qt.MetaModifier) ? "Meta+" : ""
                                            text = ctrl + alt + shift + meta + renderer.keyToAscii(event.key)
                                        }
                                        validateShortcut(this, text)
                                    }
                                }

                                Text {
                                    id: shortcutWarning
                                    color: "red"
                                }

                                onValidateShortcut: {
                                    if (shortcut.length > 0 & ActionHandler.containsShortcut(shortcut)) {
                                        shortcutWarning.text = "Shortcut already binded"
                                        source.color = "red"
                                    } else {
                                        shortcutWarning.text = ""
                                        source.color =  "black"
                                    }
                                }

                                Connections {
                                    target: configWindow
                                    onReset: {
                                        primaryShortcut.text = ActionHandler.shortcutByIndex(ActionHandler.Primary, index)
                                        secondaryShortcut.text = ActionHandler.shortcutByIndex(ActionHandler.Secondary, index)
                                    }
                                    onSave: {
                                        ActionHandler.setShortcut(ActionHandler.Primary, index, primaryShortcut.text)
                                        ActionHandler.setShortcut(ActionHandler.Secondary, index, secondaryShortcut.text)
                                    }
                                }
                            }
                        }
                    }
                }

                Item {
                    id: updatesTab

                    ColumnLayout {
                        CheckBox {
                            id: updatesEnabled
                            text: qsTr("Enabled")
                            Connections {
                                target: configWindow
                                onReset: updatesEnabled.checked = NSettings.updatesEnabled
                                onSave: NSettings.updatesEnabled = updatesEnabled.checked
                            }
                        }
                        CheckBox {
                            id: updatesUseGit
                            text: qsTr("Use Git")
                            enabled: false
                            Connections {
                                target: configWindow
                                onReset: updatesUseGit.checked = NSettings.updatesUseGit
                                onSave: NSettings.updatesUseGit = updatesUseGit.checked
                            }
                        }
                    }
                }
            }
        }

        RowLayout {
            Button {
                text: qsTr("Reset")
                onClicked: popupReset.open()
            }
            Button {
                text: qsTr("Clear history")
                onClicked: History.clear()
            }

            Item { Layout.fillWidth: true }
            Button {
                text: qsTr("Save")
                Layout.alignment: Qt.AlignRight
                onClicked: {
                    configWindow.save()
                    NSettings.save()
                    ActionHandler.save()
                    configWindow.close()
                }
            }
            Button {
                text: qsTr("Close")
                Layout.alignment: Qt.AlignRight
                onClicked: {
                    configWindow.reset()
                    configWindow.close()
                }
            }
        }
    }

    Popup {
        id: popupReset
        x: ((configWindow.width + width) / 2) - width
        y: ((configWindow.height + height) / 2) - height
        width: 300
        height: 150
        modal: true
        focus: true
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent

        ColumnLayout {
            anchors.fill: parent
            Text {
                text: qsTr("Verify reset")
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                horizontalAlignment: Text.AlignLeft
            }

            RowLayout {
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                Button {
                    text: qsTr("Reset")
                    onClicked: {
                        NSettings.resetNSettings()
                        ActionHandler.reset()
                        configWindow.reset()
                        popupReset.close()
                    }
                }
                Button {
                    text: qsTr("Cancel")
                    onClicked: popupReset.close()
                }
            }
        }
    }
}
