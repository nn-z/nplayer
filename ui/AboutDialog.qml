import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Popup {
    id: root
    x: ((parent.width + width) / 2) - width
    y: ((parent.height + height) / 2) - height
    width: 300
    height: 150
    modal: true
    focus: true
    closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent

    Text {
        textFormat: Text.RichText
        text: "<a href=\"https://gitlab.com/nn-z/nplayer\">nplayer website</a>."
        anchors.bottomMargin: 40
        anchors.topMargin: 0
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        anchors.fill: parent
        onLinkActivated: Qt.openUrlExternally(link)
    }
    Text {
        text: qsTr("Version %1").arg(renderer.applicationVersion())
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
    }
    Button {
        text: qsTr("Close")
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        onClicked: root.close()
    }
}
