import QtQuick 2.0
import QtQuick.Controls 2.2

import NPlayer 1.0

Rectangle {
    id: rectangle
    width: mainWindow.width
    height: mainWindow.height
    color: "#cc1a1a1a"
    visible: renderer.buffering

    ProgressBar {
        width: 400
        height: 32
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        indeterminate: true
    }
}
