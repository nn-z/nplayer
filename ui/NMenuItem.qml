import QtQuick 2.0
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3

import NPlayer 1.0

MenuItem {
    id: menuItem

    property NAction naction

    onNactionChanged: action = naction.primary

    function getColor() {
        if (!menuItem.enabled) return "gray"
        else if (menuItem.checkable && menuItem.checked) return "green"
        else if (menuItem.checkable && !menuItem.checked) return "darkred"
        else return "black"
    }

    function shortcut() {
        if (!action) {
            return ""
        }

        var ps = action.shortcut
        var p = (ps && ps.length > 0) ? ps : ""
        if (NSettings.menuItemShowBothShortcuts) {
            var ss = secondary.shortcut
            var s = (ss && ss.length > 0) ? ss : ""
            return (s.length > 0) ? (p + " | " + s) : p
        } else {
            return p
        }
    }

    contentItem: RowLayout {
        Text {
            id: menuText
            text: menuItem.text
            color: menuItem.getColor()
        }
        Item { Layout.fillWidth: true }
        Text {
            text: menuItem.shortcut()
            font.pointSize: menuItem.font.pointSize - 1
            color: "gray"
        }
    }

    indicator: Item {
        // Empty to allow hiding checkbox
    }
}
