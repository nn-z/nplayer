import QtQuick 2.0

import NPlayer 1.0

Rectangle {
    width: textColumn.width
    height: textColumn.height
    color: "#cc1a1a1a"
    visible: false
    enabled: visible

    property string playbackStatus
    property string position: "00:00:00"
    property string duration: "00:00:00"

    Connections {
        target: renderer
        onStatusChanged: playbackStatus.text = qsTr("Playback status: %1").arg(renderer.playbackStatusString())
        onPlaybackPositionStringChanged: position = renderer.playbackPositionString
        onPlaybackDurationChanged: duration = renderer.formatTime(renderer.playbackDuration)
    }

    Column {
        id: textColumn

        Text {
            text: qsTr("File: %1").arg(renderer.file)
            font.pointSize: 10
            color: "yellow"
        }
        Text {
            text: qsTr("Title: %1").arg(renderer.title)
            font.pointSize: 10
            color: "yellow"
        }
        Text {
            text: qsTr("Position: %1 / %2").arg(position).arg(duration)
            font.pointSize: 10
            color: "yellow"
        }
        Text {
            text: qsTr("Video dimensions: %1x%2").arg(renderer.dimensions.x).arg(renderer.dimensions.y)
            font.pointSize: 10
            color: "yellow"
        }
        Text {
            text: qsTr("HwDec: %1").arg(renderer.currentHwDec)
            font.pointSize: 10
            color: "yellow"
        }
        Text {
            text: qsTr("Cache: Used: %1KB Free: %2KB Size: %3KB").arg(renderer.cacheUsed).arg(renderer.cacheFree).arg(renderer.cacheSize)
            font.pointSize: 10
            color: "yellow"
        }
        Text {
            text: qsTr("Dropped frames: %1").arg(renderer.frameDropCount)
            font.pointSize: 10
            color: "yellow"
        }
        Text {
            text: qsTr("Mistimed frames: %1").arg(renderer.mistimedFrameCount)
            font.pointSize: 10
            color: "yellow"
        }
        Text { text: " " }
        Text {
            text: renderer.volumeMuted ? qsTr("Volume: Muted") : qsTr("Volume: %1% %2").arg(renderer.volumeLevel).arg(renderer.volumeNormalized ? qsTr("(normalized)") : "")
            font.pointSize: 10
            color: "yellow"
        }
        Text {
            text: qsTr("Window dimensions: %1x%2").arg(mainWindow.width).arg(mainWindow.height)
            font.pointSize: 10
            color: "yellow"
        }
        Text {
            id: playbackStatus
            text: qsTr("Playback status:")
            font.pointSize: 10
            color: "yellow"
        }
    }
}
