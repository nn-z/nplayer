import QtQuick 2.11
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.3

import NPlayer 1.0

PNFileDialog {
    id: dialog
    width: 768
    height: 500
    modality: Qt.WindowModal
    flags: Qt.Window | Qt.WindowTitleHint

    signal accepted

    property url url

    onWindowStateChanged: fileView.focus = true
    onPathChanged: dirPath.text = dialog.pathString()

    Shortcut {
        sequence: "Escape"
        onActivated: dialog.close()
        enabled: dialog.visible
    }

    function setTargetFile(index) {
        if (((filter === PNFileDialog.Folders) && (dialog.files[index] !== "..")) ||
            dialog.isFile(dialog.files[index])) {
            targetFile.text = dialog.files[index]
        } else {
            targetFile.text = ""
        }
    }

    ColumnLayout {
        anchors.fill: parent
        anchors.margins: 10

        RowLayout {
            Button {
                text: "↑"
                Layout.preferredWidth: height
                onClicked: {
                    dialog.parentDir()
                }
            }
            TextField {
                id: dirPath
                text: dialog.pathString()
                Layout.preferredHeight: 40
                Layout.fillWidth: true
                verticalAlignment: Text.AlignVCenter
                onAccepted: {
                    dialog.absoluteDir(text)
                }
            }
            Button {
                text: "☰"
                Layout.preferredWidth: height
                onClicked: menu.open()
                Menu {
                    id: menu
                    MenuItem {
                        id: showHidden
                        text: qsTr("Show hidden")
                        onTriggered: dialog.hidden = !dialog.hidden
                        contentItem: Text {
                            text: showHidden.text
                            font: showHidden.font
                            color: dialog.hidden ? "green" : "darkred"
                            verticalAlignment: Text.AlignVCenter
                        }
                    }
                }
            }
        }

        RowLayout {
            id: content
            spacing: 0
            Layout.fillWidth: true
            Layout.fillHeight: true

            ColumnLayout {
                id: sideItem
                Layout.fillWidth: false
                Layout.preferredWidth: 150
                Layout.fillHeight: true

                Rectangle {
                    Layout.fillWidth: true
                    Layout.preferredHeight: (systemPathsView.count + 1) * 32
                    border.color: "lightgray"
                    border.width: 1

                    ListView {
                        id: systemPathsView
                        anchors.fill: parent
                        clip: true
                        model: dialog.systemPaths

                        ScrollBar.vertical: ScrollBar {}

                        boundsBehavior: Flickable.DragOverBounds
                        highlightMoveDuration : 0
                        highlightResizeDuration: 0

                        header: Rectangle {
                            width: parent.width
                            height: 32
                            color: "lightgray"
                            Text {
                                text: qsTr("System paths")
                                anchors.fill: parent
                                verticalAlignment: Text.AlignVCenter
                                horizontalAlignment: Text.AlignHCenter
                            }
                        }

                        delegate: Item {
                            height: 32
                            width: ListView.view.width

                            Text {
                                text: dialog.systemPathString(index)
                                anchors.leftMargin: 5
                                verticalAlignment: Text.AlignVCenter
                                horizontalAlignment: Text.AlignLeft
                                anchors.fill: parent
                            }

                            MouseArea {
                                anchors.fill: parent
                                acceptedButtons: Qt.LeftButton
                                onClicked: {
                                    dialog.absoluteDir(dialog.systemPaths[index])
                                }
                            }
                        }
                    }
                }

                Rectangle {
                    id: rectangle
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    border.color: "lightgray"
                    border.width: 1

                    ColumnLayout {
                        anchors.fill: parent

                        ListView {
                            id: userPathsView
                            Layout.fillWidth: true
                            Layout.fillHeight: true
                            clip: true
                            model: dialog.userPaths

                            ScrollBar.vertical: ScrollBar {}

                            boundsBehavior: Flickable.DragOverBounds
                            highlightMoveDuration : 0
                            highlightResizeDuration: 0

                            header: Rectangle {
                                width: parent.width
                                height: 32
                                color: "lightgray"
                                Text {
                                    text: qsTr("Favorites")
                                    anchors.fill: parent
                                    verticalAlignment: Text.AlignVCenter
                                    horizontalAlignment: Text.AlignHCenter
                                }
                            }

                            delegate: Item {
                                height: 32
                                width: ListView.view.width

                                Text {
                                    text: dialog.userPathString(index)
                                    anchors.leftMargin: 5
                                    verticalAlignment: Text.AlignVCenter
                                    horizontalAlignment: Text.AlignLeft
                                    anchors.fill: parent
                                }

                                MouseArea {
                                    anchors.fill: parent
                                    acceptedButtons: Qt.LeftButton | Qt.MidButton
                                    onPressed: {
                                        if (pressedButtons & Qt.LeftButton) {
                                            dialog.absoluteDir(dialog.userPaths[index])
                                        } else if (pressedButtons & Qt.MidButton) {
                                            dialog.removeUserPath(index)
                                        }
                                    }
                                }
                            }
                        }
                    }

                    Button {
                        width: sideItem.width
                        text: "+"
                        anchors.bottom: parent.bottom
                        anchors.bottomMargin: 0
                        onClicked: dialog.addUserPath(fileView.currentIndex)
                    }
                }
            }

            MouseArea {
                Layout.preferredWidth: 6
                Layout.fillHeight: true
                acceptedButtons: Qt.LeftButton
                cursorShape: Qt.SizeHorCursor
                onMouseXChanged: {
                    if (pressed) {
                        if ((mouseX < 0 && sideItem.Layout.preferredWidth + mouseX < 100) ||
                            (mouseX > 0 && sideItem.Layout.preferredWidth + mouseX > dialog.width / 2)) {
                            return
                        }
                        sideItem.Layout.preferredWidth += mouseX
                    }
                }
            }

            Rectangle {
                Layout.fillWidth: true
                Layout.fillHeight: true
                border.color: "lightgray"
                border.width: 1

                ListView {
                    id: fileView
                    anchors.fill: parent
                    clip: true
                    model: dialog.files

                    onModelChanged: setTargetFile(0)
                    onCurrentIndexChanged: setTargetFile(currentIndex)

                    ScrollBar.vertical: ScrollBar {}

                    boundsBehavior: Flickable.DragOverBounds
                    highlightMoveDuration : 0
                    highlightResizeDuration: 0
                    highlight: Rectangle {
                        color: "lightgray"
                    }

                    delegate: Item {
                        height: 32
                        width: ListView.view.width

                        Keys.onReturnPressed: fileViewMouseArea.doubleClicked(undefined)

                        RowLayout {
                            id: row
                            anchors.fill: parent
                            anchors.leftMargin: 5
                            anchors.rightMargin: 15

                            Text {
                                text: dialog.files[index]
                                elide: Text.ElideRight
                                Layout.fillWidth: true
                                verticalAlignment: Text.AlignVCenter
                                horizontalAlignment: Text.AlignLeft
                            }

                            Text {
                                text: dialog.itemType(index)
                            }
                        }

                        MouseArea {
                            id: fileViewMouseArea
                            anchors.fill: parent
                            acceptedButtons: Qt.LeftButton
                            onClicked: {
                                fileView.focus = true
                                fileView.currentIndex = index
                                setTargetFile(index)
                            }
                            onDoubleClicked: {
                                if (filter !== PNFileDialog.Folders && dialog.isFile(dialog.files[index])) {
                                    openButton.clicked()
                                } else {
                                    dialog.relativeDir(dialog.files[index])
                                }
                            }
                        }
                    }
                }
            }
        }

        RowLayout {
            TextField {
                id: targetFile
                placeholderText: filter === PNFileDialog.Folders ? qsTr("Select folder") : qsTr("Select file")
                readOnly: true
                activeFocusOnPress: false
                Layout.preferredHeight: 40
                Layout.fillWidth: true
                verticalAlignment: Text.AlignVCenter
            }
            Item { Layout.fillWidth: true }
            Button {
                id: openButton
                text: qsTr("Open")
                enabled: targetFile.text.length > 0
                Layout.alignment: Qt.AlignRight
                onClicked: {
                    url = dialog.convertToUrl(targetFile.text)
                    accepted()
                    dialog.close()
                }
            }
            Button {
                text: qsTr("Cancel")
                Layout.alignment: Qt.AlignRight
                onClicked: {
                    dialog.close()
                }
            }
        }
    }
}
