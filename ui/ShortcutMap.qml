import QtQuick 2.0
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3

import NPlayer 1.0

Rectangle {
    id: map
    width: 400
    height: mainWindow.height
    color: "#cc1a1a1a"
    x: -width
    enabled: false

    onEnabledChanged: mainWindow.actionsEnabled = !enabled

    states: [
        State {
            name: "show"
            when: enabled
            PropertyChanges {
                target: map
                x: 0
            }
        },
        State {
            name: "hide"
            when: !enabled
            PropertyChanges {
                target: map
                x: -width
            }
        }
    ]

    transitions: Transition {
        PropertyAnimation {
            properties: "x"
            duration: 200
        }
    }

    ScrollView {
        anchors.fill: parent
        clip: true
        ColumnLayout {
            Layout.fillWidth: true
            spacing: 1
            RowLayout {
                Text {
                    Layout.preferredWidth: 200
                    text: qsTr("Action")
                    color: "white"
                    font.underline: true
                }
                Text {
                    Layout.preferredWidth: 100
                    text: qsTr("Primary")
                    color: "white"
                    font.underline: true
                }
                Text {
                    Layout.preferredWidth: 100
                    text: qsTr("Secondary")
                    color: "white"
                    font.underline: true
                }
            }

            Repeater {
                model: ActionHandler.num()
                RowLayout {
                    Text {
                        Layout.preferredWidth: 200
                        text: ActionHandler.actionByIndex(index)
                        color: "white"
                    }

                    Text {
                        Layout.preferredWidth: 100
                        text: ActionHandler.shortcutByIndex(ActionHandler.Primary, index)
                        color: "white"
                    }

                    Text {
                        Layout.preferredWidth: 100
                        text: ActionHandler.shortcutByIndex(ActionHandler.Secondary, index)
                        color: "white"
                    }
                }
            }
        }
    }

    MouseArea {
        width: mainWindow.width
        height: mainWindow.height
        acceptedButtons: Qt.LeftButton | Qt.RightButton
        onClicked: map.enabled = false
    }

    Action {
        shortcut: "Escape"
        onTriggered: map.enabled = false
        enabled: map.enabled
    }
}
