import QtQuick 2.0

import NPlayer 1.0

Rectangle {
    id: rect
    x: ((mainWindow.width + width) / 2) - width
    y: ((mainWindow.height + height) / 2) - height
    width: 250
    height: 100
    visible: false
    border.color: "black"
    border.width: 1

    Connections {
        target: renderer
        onErrorMsgChanged: {
            errorText.text = renderer.errorMsg
            visible = true
            errorTimer.start()
        }
    }

    Timer {
        id: errorTimer
        interval: 5000
        running: false
        repeat: false
        onTriggered: rect.visible = false
    }

    Text {
        id: errorText
        anchors.fill: parent
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        onTextChanged: {
            rect.width =  implicitWidth + 16
        }
    }
}
