import QtQuick 2.0
import QtGraphicalEffects 1.0

import NPlayer 1.0

Item {
    width: parent.width
    height: parent.height
    visible: renderer.status === MpvObject.PS_Idle

    ConicalGradient {
        id: backgroundGradient
        anchors.fill: parent
        gradient: Gradient {
            GradientStop {
               position: 0.000
               color: Qt.rgba(1, 0, 0, 1)
            }
            GradientStop {
               position: 0.333
               color: Qt.rgba(0, 1, 0, 1)
            }
            GradientStop {
               position: 0.667
               color: Qt.rgba(0, 0, 1, 1)
            }
            GradientStop {
               position: 1.000
               color: Qt.rgba(1, 0, 0, 1)
            }
        }
    }

    Image {
        id: backgroundImage
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        width: 256
        height: 256
        source: "icons/play.png"
    }
}
