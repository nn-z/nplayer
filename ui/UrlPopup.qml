import QtQuick 2.0
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3

Popup {
    id: popupURL
    x: ((mainWindow.width + width) / 2) - width
    y: ((mainWindow.height + height) / 2) - height
    width: 450
    height: 150
    modal: true
    focus: true
    closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent

    onOpened: textField.focus = true
    onClosed: textField.text = ""

    ColumnLayout {
        anchors.fill: parent

        TextField {
            id: textField
            Layout.fillWidth: true
            placeholderText: qsTr("URL")
        }

        RowLayout {
            Layout.alignment: Qt.AlignHCenter
            Text {
                text: qsTr("Max resolution ")
            }

            ComboBox {
                id: resolution
                model: ListModel {
                    id: model
                    ListElement { text: qsTr("Screen") }
                    ListElement { text: "480" }
                    ListElement { text: "720" }
                    ListElement { text: "1080" }
                    ListElement { text: "1440" }
                    ListElement { text: "2160" }
                }
            }
        }

        RowLayout {
            Layout.alignment: Qt.AlignHCenter
            Button {
                text: qsTr("Accept")
                onClicked: {
                    renderer.loadUrl(textField.text, resolution.currentIndex)
                    popupURL.close()
                }
            }
            Button {
                text: qsTr("Close")
                onClicked: popupURL.close()
            }
        }
    }
}
