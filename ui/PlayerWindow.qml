import QtQuick 2.10
import QtQuick.Window 2.10
import QtQuick.Dialogs 1.3
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0
import QtQuick.Controls.Styles 1.4
import QtQml 2.2

import NPlayer 1.0

Window {
    id: mainWindow
    visible: true
    width: 1280
    height: 720
    title: {
        if (renderer.title.length > 0)
            "nplayer - %1".arg(renderer.title)
        else
            "nplayer"
    }

    property bool actionsEnabled: true

    // scale mainWindow using renderer.dimensions as base
    function scaleWindow(percent) {
        var vd = renderer.dimensions
        mainWindow.width = vd.x * (percent / 100)
        mainWindow.height = vd.y  * (percent / 100)
    }

    // os specific components
    Loader {
        Component.onCompleted: {
            if (Qt.platform.os === "windows") {
                source = "WindowsTaskbarButton.qml"
            }
        }
    }
    Loader {
        id: windowsUpdater
        Component.onCompleted: {
            if (Qt.platform.os === "windows") {
                source = "WindowsUpdater.qml"
            }
        }
    }

    MpvRenderer {
        id: renderer
        objectName: "renderer"
    }

    PlayerBackground {
        id: playerBackground
    }

    Buffer {
        id: buffer
    }

    ErrorDialog {
        id: errorDialog
    }

    DropArea {
        anchors.fill: parent
        onDropped: {
            if (renderer.fileIsSubtitle(drop.urls[0])) {
                renderer.addSubtitleFile([0])
            } else {
                var files = renderer.removeNonVideoFiles(drop.urls)
                Playlist.filesDropped(files)
            }
        }
    }

    Item {
        id: dialogs

        function open(filter) {
            if (NSettings.uiNativeFileDialog) {
                fileDialogNative.title = (filter & NFileDialog.Folders) ? qsTr("Choose folder") : qsTr("Choose file")
                fileDialog.filter = filter

                switch (filter) {
                case NFileDialog.VideoFiles:
                    fileDialogNative.nameFilters = renderer.getVideoFilters()
                    fileDialogNative.selectFolder = false
                    break
                case NFileDialog.SubtitleFiles:
                    fileDialogNative.nameFilters = renderer.getSubtitleFilters()
                    fileDialogNative.selectFolder = false
                    break
                case NFileDialog.Folders:
                    fileDialogNative.nameFilters = ""
                    fileDialogNative.selectFolder = true
                    break
                }

                fileDialogNative.open()
            } else {
                fileDialog.title = (filter & NFileDialog.Folders) ? qsTr("Choose folder") : qsTr("Choose file")
                fileDialog.filter = filter
                fileDialog.show()
            }
        }

        function _accepted(item) {
            switch (fileDialog.filter) {
            case NFileDialog.VideoFiles:
                Playlist.populateFromFile(item)
                break
            case NFileDialog.SubtitleFiles:
                renderer.addSubtitleFile(item)
                break
            case NFileDialog.Folders:
                Playlist.populateFromFolder(item)
                break
            }
        }

        NFileDialog {
            id: fileDialog
            onAccepted: dialogs._accepted(url)
        }

        FileDialog {
            id: fileDialogNative
            folder: shortcuts.home
            onAccepted: dialogs._accepted(fileUrl)
        }
    }

    OnScreenDisplay {
        id: onScreenDisplay
    }

    OnScreenControls {
        id: onScreenControls
    }

    OnScreenHint {
        id: onScreenHint
    }

    OnScreenSeekBar {
        id: onScreenSeekBar
    }

    PlaylistView {
        id: playlistView
    }

    HistoryView {
        id: historyView
    }

    ContextMenu {
        id: contextMenu
    }

    Item {
        id: configWindow
        property var _config
        function show() {
            if (_config === undefined) {
                var com = Qt.createComponent("ConfigWindow.qml")
                _config = com.createObject(mainWindow)
            }
            _config.show()
        }
    }

    Item {
        id: about
        property var _about
        function open() {
            if (_about === undefined) {
                var com = Qt.createComponent("AboutDialog.qml")
                _about = com.createObject(mainWindow)
            }
            _about.open()
        }
    }

    ShortcutMap {
        id: shortcutMap
    }

    UrlPopup {
        id: urlPopup
    }

    DebugPropertyPopup {
        id: popupDebugProperty
    }
}
