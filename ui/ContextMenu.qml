import QtQuick 2.0
import QtQuick.Controls 2.3
import QtQuick.Window 2.10
import QtQml 2.11

import NPlayer 1.0

Item {
    property alias menu: menu

    NAction {
        id: actionShortcutMap
        text: qsTr("Shortcut map")
        action: "showShortcutMap"
        onTriggered: shortcutMap.enabled = !shortcutMap.enabled
    }
    NAction {
        id: actionOpen
        text: qsTr("Open")
        action: "openFile"
        onTriggered: dialogs.open(NFileDialog.VideoFiles)
    }
    NAction {
        id: actionOpenFolder
        text: qsTr("Open Folder")
        action: "openFolder"
        onTriggered: dialogs.open(NFileDialog.Folders)
    }
    NAction {
        id: actionOpenURL
        text: qsTr("Open URL")
        action: "openURL"
        enabled: renderer.networkStreamingSupported()
        onTriggered: urlPopup.open()
    }
    NAction {
        id: actionShuffle
        text: qsTr("Shuffle")
        action: "shuffle"
        hint: true
        hintText: qsTr("Shuffle: %1").arg(Playlist.shuffle)
        checkable: true
        checked: Playlist.shuffle
        onTriggered: Playlist.shuffle = !Playlist.shuffle
    }
    NAction {
        id: actionPlay
        text: qsTr("Play/Pause")
        action: "playToggle"
        onTriggered: {
            if (renderer.status > MpvObject.PS_Idle) {
                renderer.togglePause()
                onScreenHint.show(renderer.status === MpvObject.PS_Playing ? qsTr("Play") : qsTr("Pause"))
            } else if (renderer.tryLoadFromLastFile()) {
                onScreenHint.show(qsTr("Load last file"))
            } else {
                onScreenHint.show(qsTr("Unable to load last file"))
            }
        }
    }
    NAction {
        id: actionPrevItem
        text: qsTr("Prev Item")
        action: "prevItem"
        hint: true
        onTriggered: Playlist.prevFile()
    }
    NAction {
        id: actionNextItem
        text: qsTr("Next Item")
        action: "nextItem"
        hint: true
        onTriggered: Playlist.nextFile(false)
    }
    NAction {
        id: actionSeekShortForward
        text: qsTr("Seek +Short")
        action: "seek+short"
        hint: true
        onTriggered: {
            renderer.seekPlayback(NSettings.seekShort)
            onScreenSeekBar.show()
        }
    }
    NAction {
        id: actionSeekShortBackward
        text: qsTr("Seek -Short")
        action: "seek-short"
        hint: true
        onTriggered: {
            renderer.seekPlayback(-NSettings.seekShort)
            onScreenSeekBar.show()
        }
    }
    NAction {
        id: actionSeekMediumForward
        text: qsTr("Seek +Medium")
        action: "seek+medium"
        hint: true
        onTriggered: {
            renderer.seekPlayback(NSettings.seekMedium)
            onScreenSeekBar.show()
        }
    }
    NAction {
        id: actionSeekMediumBackward
        text: qsTr("Seek -Medium")
        action: "seek-medium"
        hint: true
        onTriggered: {
            renderer.seekPlayback(-NSettings.seekMedium)
            onScreenSeekBar.show()
        }
    }
    NAction {
        id: actionSeekLongForward
        text: qsTr("Seek +Long")
        action: "seek+long"
        hint: true
        onTriggered: {
            renderer.seekPlayback(NSettings.seekLong)
            onScreenSeekBar.show()
        }
    }
    NAction {
        id: actionSeekLongtBackward
        text: qsTr("Seek -Long")
        action: "seek-long"
        hint: true
        onTriggered: {
            renderer.seekPlayback(-NSettings.seekLong)
            onScreenSeekBar.show()
        }
    }
    NAction {
        id: actionClearPlaylist
        text: qsTr("Clear playlist")
        action: "clearPlaylist"
        hint: true
        onTriggered: Playlist.clearPlaylist()
    }
    NAction {
        id: actionFullscreen
        text: qsTr("Fullscreen")
        action: "fullscreen"
        hint: true
        hintText: mainWindow.visibility === Window.FullScreen ? qsTr("Fullscreen") :qsTr("Windowed")
        checkable: true
        checked: mainWindow.visibility === Window.FullScreen
        onTriggered:  {
            mainWindow.visibility = (mainWindow.visibility === Window.FullScreen) ? Window.Windowed : Window.FullScreen
        }
    }
    NAction {
        id: actionScale100
        text: qsTr("Scale 100%")
        action: "scale100%"
        hint: true
        onTriggered: mainWindow.scaleWindow(100)
    }
    NAction {
        id: actionScale125
        text: qsTr("Scale 125%")
        action: "scale125%"
        hint: true
        onTriggered: mainWindow.scaleWindow(125)
    }
    NAction {
        id: actionScale150
        text: qsTr("Scale 150%")
        action: "scale150%"
        hint: true
        onTriggered: mainWindow.scaleWindow(150)
    }
    NAction {
        id: actionScale200
        text: qsTr("Scale 200%")
        action: "scale200%"
        hint: true
        onTriggered: mainWindow.scaleWindow(200)
    }
    NAction {
        id: actionVolumeUp
        text: qsTr("Volume +%1").arg(NSettings.volumeStep)
        enabled: !playlistView.enabled && !historyView.enabled
        action: "volumeStepUp"
        hint: true
        hintText: qsTr("Volume: %1%").arg(renderer.volumeLevel)
        onTriggered: renderer.volumeLevel += NSettings.volumeStep
    }
    NAction {
        id: actionVolumeDown
        text: qsTr("Volume -%1").arg(NSettings.volumeStep)
        enabled: !playlistView.enabled && !historyView.enabled
        action: "volumeStepDown"
        hint: true
        hintText: qsTr("Volume: %1%").arg(renderer.volumeLevel)
        onTriggered: renderer.volumeLevel -= NSettings.volumeStep
    }
    NAction {
        id: actionVolumeMute
        text: qsTr("Mute")
        action: "volumeMute"
        hint: true
        checkable: true
        checked: renderer.volumeMuted
        onTriggered: renderer.volumeMuted = !checked
    }
    NAction {
        id: actionShowConfig
        text: qsTr("Config")
        action: "showConfig"
        onTriggered: configWindow.show()
    }
    NAction {
        id: actionShowPlaylist
        text: qsTr("Playlist")
        action: "showPlaylist"
        checkable: true
        checked: playlistView.enabled
        onTriggered: playlistView.enabled = !playlistView.enabled
    }
    NAction {
        id: actionShowHistory
        text: qsTr("History")
        action: "showHistory"
        checkable: true
        checked: historyView.enabled
        onTriggered: historyView.toggle()
    }
    NAction {
        id: actionShowOnScreenDisplay
        text: qsTr("On Screen Display")
        action: "showOnScreenDisplay"
        checkable: true
        checked: onScreenDisplay.visible
        onTriggered: onScreenDisplay.visible = !onScreenDisplay.visible
    }
    NAction {
        id: actionShowOnScreenControls
        text: qsTr("On Screen Controls")
        action: "showOnScreenControls"
        checkable: true
        checked: NSettings.onScreenControls
        onTriggered: NSettings.onScreenControls = !NSettings.onScreenControls
    }
    NAction {
        id: actionShowonScreenSeekBar
        text: qsTr("On Screen Seek Bar")
        action: "showonScreenSeekBar"
        checkable: true
        checked: NSettings.onScreenSeekBar
        onTriggered: NSettings.onScreenSeekBar = !NSettings.onScreenSeekBar
    }
    NAction {
        id: actionShowOnScreenHint
        text: qsTr("On Screen Hint")
        action: "showOnScreenHint"
        checkable: true
        checked: NSettings.onScreenHint
        onTriggered: NSettings.onScreenHint = !NSettings.onScreenHint
    }
    NAction {
        id: actionQuit
        text: qsTr("Quit")
        action: "quit"
        onTriggered: Qt.quit()
    }
    NAction {
        id: actionSubtitleAdd
        enabled: renderer.status > MpvObject.PS_Idle
        text: qsTr("Add subtitle")
        action: "addSubtitle"
        onTriggered: dialogs.open(NFileDialog.SubtitleFiles)
    }
    NAction {
        id: actionSubtitleRemove
        enabled: renderer.status > MpvObject.PS_Idle
        text: qsTr("Remove subtitle")
        action: "removeSubtitle"
        onTriggered: renderer.removeSubtitle()
    }
    NAction {
        id: actionSubtitleEnabled
        enabled: renderer.status > MpvObject.PS_Idle
        text: qsTr("Show subtitles")
        action: "showSubtitles"
        hint: true
        checkable: true
        checked: renderer.subVisible
        onTriggered: renderer.subVisible = !renderer.subVisible
    }
    NAction {
        id: actionSubtitleNextTrack
        text: qsTr("Next subtitle track")
        action: "nextSubtitleTrack"
        hint: true
        enabled: renderer.subNames.length > 0
        onTriggered: renderer.nextSubtitleTrack()
    }
    NAction {
        id: actionAudioNextTrack
        text: qsTr("Next audio track")
        action: "nextAudioTrack"
        hint: true
        enabled: renderer.audioNames.length > 0
        onTriggered: renderer.nextAudioTrack()
    }
    NAction {
        id: actionClearHistory
        text: qsTr("Clear History")
        action: "clearHistory"
        hint: true
        onTriggered: History.clear()
    }
    NAction {
        id: actionStreamQualityScreen
        text: qsTr("Screen")
        action: "streamQualtyScreen"
        checkable: true
        checked: renderer.streamResolution === MpvObject.NSR_Screen
        hint: true
        hintText: qsTr("Stream quality screen")
        enabled: renderer.streamActive
        onTriggered: renderer.setStreamResolution(0)
    }
    NAction {
        id: actionStreamQuality480
        text: qsTr("480p")
        action: "streamQualty480"
        checkable: true
        checked: renderer.streamResolution === MpvObject.NSR_P480
        hint: true
        hintText: qsTr("Stream quality 480p")
        enabled: renderer.streamActive
        onTriggered: renderer.setStreamResolution(1)
    }
    NAction {
        id: actionStreamQuality720
        text: qsTr("720p")
        action: "streamQualty720"
        checkable: true
        checked: renderer.streamResolution === MpvObject.NSR_P720
        hint: true
        hintText: qsTr("Stream quality 720p")
        enabled: renderer.streamActive
        onTriggered: renderer.setStreamResolution(2)
    }
    NAction {
        id: actionStreamQuality1080
        text: qsTr("1080p")
        action: "streamQualty1080"
        checkable: true
        checked: renderer.streamResolution === MpvObject.NSR_P1080
        hint: true
        hintText: qsTr("Stream quality 1080p")
        enabled: renderer.streamActive
        onTriggered: renderer.setStreamResolution(3)
    }
    NAction {
        id: actionStreamQuality1440
        text: qsTr("1440p")
        action: "streamQualty1440"
        checkable: true
        checked: renderer.streamResolution === MpvObject.NSR_P1440
        hint: true
        hintText: qsTr("Stream quality 1440p")
        enabled: renderer.streamActive
        onTriggered: renderer.setStreamResolution(4)
    }
    NAction {
        id: actionStreamQuality2160
        text: qsTr("2160p")
        action: "streamQualty2160"
        checkable: true
        checked: renderer.streamResolution === MpvObject.NSR_P2160
        hint: true
        hintText: qsTr("Stream quality 2160p")
        enabled: renderer.streamActive
        onTriggered: renderer.setStreamResolution(5)
    }
    NAction {
        id: actionDebugProperty
        text: qsTr("Debug property")
        action: "debugProperty"
        onTriggered: popupDebugProperty.open()
    }
    NAction {
        id: actionVolumeNormalize
        text: qsTr("Normalize")
        action: "volumeNormalize"
        checkable: true
        checked: renderer.volumeNormalized
        hint: true
        onTriggered: renderer.volumeNormalized = !renderer.volumeNormalized
    }
    NAction {
        id: actionSubOverrideStyle
        text: qsTr("Override style")
        action: "subOverrideStyle"
        checkable: true
        checked: renderer.subOverrideStyle
        hint: true
        onTriggered: renderer.subOverrideStyle = !renderer.subOverrideStyle
    }

    NMenu {
        id: menu
        NMenuItem { naction: actionOpen }
        NMenuItem { naction: actionOpenFolder }
        NMenuItem { naction: actionOpenURL }
        MenuSeparator { }
        NMenu {
            title: qsTr("Playback")
            NMenuItem { naction: actionPlay }
            NMenu {
                title: qsTr("Seek")
                NMenuItem { naction: actionSeekShortForward }
                NMenuItem { naction: actionSeekMediumForward }
                NMenuItem { naction: actionSeekLongForward }
                NMenuItem { naction: actionSeekShortBackward }
                NMenuItem { naction: actionSeekMediumBackward }
                NMenuItem { naction: actionSeekLongtBackward }
            }
            NMenu {
                title: qsTr("Playlist")
                NMenuItem { naction: actionShowPlaylist }
                MenuSeparator { }
                NMenuItem { naction: actionShuffle }
                MenuSeparator { }
                NMenuItem { naction: actionPrevItem }
                NMenuItem { naction: actionNextItem }
                MenuSeparator { }
                NMenuItem { naction: actionClearPlaylist }
            }
            NMenu {
                title: qsTr("History")
                NMenuItem { naction: actionShowHistory }
                MenuSeparator { }
                NMenuItem { naction: actionClearHistory }
            }
        }
        NMenu {
            title: qsTr("Video")
            NMenuItem { naction: actionFullscreen }
            MenuSeparator { }
            NMenu {
                title: qsTr("Scale")
                enabled: mainWindow.visibility === Window.Windowed
                NMenuItem { naction: actionScale100 }
                NMenuItem { naction: actionScale125 }
                NMenuItem { naction: actionScale150 }
                NMenuItem { naction: actionScale200 }
            }
            NMenu {
                title: qsTr("Stream quality")
                enabled: renderer.streamActive
                NMenuItem { naction: actionStreamQualityScreen }
                NMenuItem { naction: actionStreamQuality480 }
                NMenuItem { naction: actionStreamQuality720 }
                NMenuItem { naction: actionStreamQuality1080 }
                NMenuItem { naction: actionStreamQuality1440 }
                NMenuItem { naction: actionStreamQuality2160 }
            }
        }
        NMenu {
            title: qsTr("Audio")
            id: audioMenu
            NMenuItem { naction: actionVolumeUp }
            NMenuItem { naction: actionVolumeDown }
            MenuSeparator { }
            NMenuItem { naction: actionVolumeMute }
            NMenuItem { naction: actionVolumeNormalize }
            MenuSeparator { }
            NMenuItem { naction: actionAudioNextTrack }
            MenuSeparator { visible: renderer.audioNames.length > 0 }
            Instantiator {
                model: renderer.audioNames.length
                onObjectAdded: audioMenu.insertItem(audioMenu.count, object)
                onObjectRemoved: audioMenu.removeItem(object)
                delegate: NMenuItem {
                    text: "[%1] %2".arg(index).arg(renderer.audioNames[index])
                    checkable: true
                    checked: renderer.audioId === index + 1
                    onTriggered: renderer.setAudioId(index + 1)
                }
            }
        }
        NMenu {
            title: qsTr("Subtitle")
            id: subtitleMenu
            enabled: renderer.status > MpvObject.PS_Idle
            NMenuItem { naction: actionSubtitleAdd }
            NMenuItem { naction: actionSubtitleRemove }
            MenuSeparator { }
            NMenuItem { naction: actionSubtitleEnabled }
            MenuSeparator { }
            NMenuItem { naction: actionSubOverrideStyle }
            MenuSeparator { }
            NMenuItem { naction: actionSubtitleNextTrack }
            MenuSeparator { visible: renderer.subNameslength > 0 }
            Instantiator {
                model: renderer.subNames.length
                onObjectAdded: subtitleMenu.insertItem(subtitleMenu.count, object)
                onObjectRemoved: subtitleMenu.removeItem(object)
                delegate: NMenuItem {
                    text: "[%1] %2".arg(index).arg(+ renderer.subNames[index])
                    checkable: true
                    checked: renderer.subId === index + 1
                    onTriggered: renderer.setSubId(index + 1)
                }
            }
        }
        NMenu {
            title: qsTr("Options")
            NMenuItem { naction: actionShowConfig }
            MenuSeparator { }
            NMenuItem { naction: actionShowOnScreenDisplay }
            NMenuItem { naction: actionShowOnScreenControls }
            NMenuItem { naction: actionShowonScreenSeekBar }
            NMenuItem { naction: actionShowOnScreenHint }
            NMenu {
                title: qsTr("Misc")
                NMenuItem { naction: actionShortcutMap }
                NMenuItem { naction: actionDebugProperty }
            }
            MenuSeparator { }
            NMenuItem {
                text: qsTr("About")
                onTriggered: about.open()
            }
        }
        MenuSeparator { }
        NMenuItem { naction: actionQuit }
    }
}
