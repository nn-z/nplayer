import QtQuick 2.0
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3

ColumnLayout {
    id: root

    property string fontFamily
    property string fontSize

    function setFontFamily(family) {
        fontFamily = family
        preview.font.family = fontFamily

        for (var i = 0; i < familyView.count; i++) {
            if (familyView.model[i] === family) {
                familyView.currentIndex = i
                break
            }
        }
    }

    function setFontSize(size) {
        fontSize = size
        preview.font.pointSize = fontSize
        sizeView.currentIndex = size - 1
    }

    RowLayout {
        Rectangle {
            Layout.fillWidth: true
            Layout.fillHeight: true
            border.color: "lightgray"
            border.width: 1

            ListView {
                id: familyView
                anchors.fill: parent
                clip: true
                model: Qt.fontFamilies()

                ScrollBar.vertical: ScrollBar {}

                highlightMoveDuration : 0
                highlightResizeDuration: 0
                highlight: Rectangle {
                    color: "lightgray"
                }

                delegate: Item {
                    height: 32
                    width: ListView.view.width

                    Text {
                        id: familyText
                        anchors.centerIn: parent
                        text: modelData
                        font.family: Qt.fontFamilies()[index]
                    }

                    MouseArea {
                        anchors.fill: parent
                        acceptedButtons: Qt.LeftButton
                        onClicked: {
                            familyView.currentIndex = index
                            fontFamily = Qt.fontFamilies()[index]
                            preview.font.family = fontFamily
                        }
                    }
                }
            }
        }

        Rectangle {
            Layout.preferredWidth: 60
            Layout.fillHeight: true
            border.color: "lightgray"
            border.width: 1

            ListView {
                id: sizeView
                anchors.fill: parent
                clip: true
                model: 32

                ScrollBar.vertical: ScrollBar {}

                highlightMoveDuration : 0
                highlightResizeDuration: 0
                highlight: Rectangle {
                    color: "lightgray"
                }

                delegate: Item {
                    height: 32
                    width: ListView.view.width

                    Text {
                        anchors.centerIn: parent
                        text: index + 1
                    }

                    MouseArea {
                        anchors.fill: parent
                        acceptedButtons: Qt.LeftButton
                        onClicked: {
                            sizeView.currentIndex = index
                            fontSize = index + 1
                            preview.font.pointSize = fontSize
                        }
                    }
                }
            }
        }
    }

    TextField {
        id: preview
        readOnly: true
        activeFocusOnPress: false
        Layout.preferredHeight: 40
        Layout.fillWidth: true
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
        text: qsTr("The quick brown fox jumps over the lazy dog")
    }
}
