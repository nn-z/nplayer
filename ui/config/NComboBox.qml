import QtQuick 2.0
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3

RowLayout {
    property alias label: textId.text
    property alias model: comboBox.model
    property alias currentIndex: comboBox.currentIndex

    ToolTip.visible: comboBox.hovered && ToolTip.text.length > 0
    ToolTip.delay: 500

    Text {
        id: textId
    }

    ComboBox {
        id: comboBox
    }
}
