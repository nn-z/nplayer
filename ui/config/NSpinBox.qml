import QtQuick 2.0
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3

RowLayout {
    property alias label: textId.text
    property alias value: spinBox.value
    property alias from: spinBox.from
    property alias to: spinBox.to

    ToolTip.visible: spinBox.hovered && ToolTip.text.length > 0
    ToolTip.delay: 500

    Text {
        id: textId
    }

    SpinBox {
        id: spinBox
        editable: true
        from: 0
        to: 60000
    }
}
