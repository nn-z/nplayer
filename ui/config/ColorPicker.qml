import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.2

RowLayout {
    property alias label: textId.text
    property alias color: rect.color

    Text {
        id: textId
    }

    Rectangle {
        id: rect
        Layout.preferredWidth: 32
        Layout.preferredHeight: 32
        border.color: "black"
        border.width: 1

        MouseArea {
            anchors.fill: parent
            acceptedButtons: Qt.LeftButton
            onClicked: {
                colorDialog.open()
            }
        }
    }

    ColorDialog {
        id: colorDialog
        title: "Choose a color"
        onAccepted: rect.color = color
    }
}
