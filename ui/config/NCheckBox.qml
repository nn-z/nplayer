import QtQuick 2.0
import QtQuick.Controls 2.2

CheckBox {
    ToolTip.visible: hovered && ToolTip.text.length > 0
    ToolTip.delay: 500
}
