import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

RowLayout {
    id: root
    property alias label: textId.text
    property alias text: textFieldId.text
    property alias placeholderText: textFieldId.placeholderText
    property alias readOnly: textFieldId.readOnly

    signal pressed(var event)

    Text {
        id: textId
    }

    TextField {
        id: textFieldId
        Keys.onPressed: root.pressed(event)
    }
}
