import QtQuick 2.0
import QtQuick.Controls 2.2

import NPlayer 1.0

Item {
    id: seekBar
    anchors.horizontalCenter: parent.horizontalCenter
    anchors.verticalCenter: parent.verticalCenter
    width: parent.width
    visible: false
    enabled: visible

    function show() {
        if (renderer.status > MpvObject.PS_Idle && NSettings.onScreenSeekBar
                && NSettings.onScreenSeekBarHideTime > 0) {
            renderer.updateDelayedProperties()
            progress.value = renderer.playbackPositionNormalized
            seekBar.visible = true
            seekBarTimer.restart()
        }
    }

    Timer {
        id: seekBarTimer
        interval: NSettings.onScreenSeekBarHideTime
        running: false
        repeat: false
        onTriggered: seekBar.visible = false
    }

    ProgressBar {
        id: progress
        width: parent.width - 200
        height: 32
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        opacity: NSettings.onScreenSeekBarOpacity

        background: Rectangle {
            anchors.fill: parent
            color: "#cc1a1a1a"
        }

        contentItem: Item {
            anchors.fill: parent
            anchors.margins: 4

            Rectangle {
                width: progress.visualPosition * parent.width
                height: parent.height
                color: "white"
            }
        }
    }
}
