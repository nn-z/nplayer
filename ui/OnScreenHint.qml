import QtQuick 2.0

import NPlayer 1.0

Item {
    id: hintItem
    width: 300
    height: 100
    visible: true

    function show(hint) {
        if (NSettings.onScreenHint && NSettings.onScreenHintHideTime > 0 && !onScreenDisplay.visible) {
            hintText.text = hint
            hintItem.visible = true
            hintTimer.restart()
        }
    }

    Timer {
        id: hintTimer
        interval: NSettings.onScreenHintHideTime
        running: false
        repeat: false
        onTriggered: hintItem.visible = false
    }

    Text {
        id: hintText
        font.pointSize: 10
        color: "white"
    }
}
