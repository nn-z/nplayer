import QtQuick 2.11
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3

import NPlayer 1.0

Rectangle {
    id: rect
    width: 0
    height: parent.height
    anchors.right: parent.right
    anchors.rightMargin: 0
    color: "#cc1a1a1a"
    state: "hide"
    enabled: false

    onEnabledChanged: {
        if (!enabled) {
            playlistView.setFocus()
        } else {
            view.focus = true
        }
    }

    function setFocus() {
        if (enabled) {
            view.focus = true
        }
    }

    function toggle() {
        state = (state === "show") ? "hide" : "show"
        enabled = (state === "show")
    }

    states: [
        State {
            name: "show"
            PropertyChanges {
                target: rect
                width: 400
            }
        },
        State {
            name: "hide"
            PropertyChanges {
                target: rect
                width: 0
            }
        }
    ]

    transitions: Transition {
        PropertyAnimation {
            properties: "width"
            duration: 200
        }
    }

    Shortcut {
        sequence: "Escape"
        onActivated: {
            if (stackView.currentIndex === 1) {
                stackView.currentIndex = !stackView.currentIndex
            } else {
                rect.state = "hide"
            }
        }
        enabled: view.focus
    }

    MouseArea {
        anchors.fill: parent
        acceptedButtons: Qt.MiddleButton
        hoverEnabled: true
        onPositionChanged: {
            if (pressed) {
                var wantedWidth = Math.max(Math.min((mainWindow.width - rect.x) - mouseX, mainWindow.width), 400)
                rect.width = Math.min(wantedWidth, mainWindow.width / 2)
            }
        }
    }

    RowLayout {
        height: 32
        Button {
            Layout.preferredHeight: 32
            text: qsTr("Search")
            onClicked: stackView.currentIndex = !stackView.currentIndex
        }
        StackLayout {
            id: stackView
            Layout.preferredHeight: 32
            Layout.fillWidth: true
            Layout.fillHeight: true

            onCurrentIndexChanged: {
                searchField.text = ""
                History.clearFilter()
                if (currentIndex == 1) {
                    searchField.focus = true
                }
            }

            Text {
                text: qsTr("History")
                verticalAlignment: Text.AlignTop
                horizontalAlignment: Text.AlignHCenter
                font.underline: true
                color: "white"
            }
            TextField {
                id: searchField
                Layout.fillWidth: true
                Layout.fillHeight: true
                onTextChanged: {
                    History.filter(text)
                }
            }
        }
        Button {
            Layout.preferredHeight: 32
            text: qsTr("Clear")
            onClicked: History.clear()
        }
    }

    function tryLoadFile(url)
    {
        if (renderer.fileExists(url)) {
            Playlist.populateFromFile(url)
        }
    }

    ListView {
        id: view
        anchors.fill: parent
        clip: true
        anchors.topMargin: 32
        currentIndex: 0

        ScrollBar.vertical: ScrollBar {}

        boundsBehavior: Flickable.DragOverBounds
        highlightMoveDuration : 0
        highlightResizeDuration: 0
        highlight: Rectangle {
            color: "gray"
        }

        model: History

        delegate: Item {
            width: view.height
            height: itemFileName.height + itemFilePath.height

            Keys.onReturnPressed: {
                view.currentIndex = index
                tryLoadFile(History.urlAt(index))
            }

            Column {
                Text {
                    id: itemFileName
                    text: model.fileName
                    color: !model.modelEnabled ? "darkgray" : model.fileExists ? "white" : "darkred"
                }
                Row {
                    Text {
                        id: itemFileDate
                        text: model.fileDate
                        color: "#666666"
                    }
                    Text {
                        id: itemFilePath
                        text: model.filePath
                        color: "#666666"
                    }
                }
            }

            MouseArea {
                anchors.fill: parent
                acceptedButtons: Qt.LeftButton | Qt.RightButton
                onClicked: {
                    if (mouse.button === Qt.LeftButton) {
                        view.currentIndex = index
                    }
                }
                onDoubleClicked: {
                    if (mouse.button === Qt.LeftButton) {
                        view.currentIndex = index
                        tryLoadFile(History.urlAt(index))
                    }
                }
            }
        }
    }
}
