import QtQuick 2.0
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3

import NPlayer 1.0

Rectangle {
    id: controls
    width: 480
    height: 42
    anchors.horizontalCenter: parent.horizontalCenter
    anchors.bottom: parent.bottom
    anchors.bottomMargin: -height
    color: "#cc1a1a1a"
    enabled: renderer.status > MpvObject.PS_Idle

    states: [
        State {
            name: "show"
            PropertyChanges {
                target: controls
                anchors.bottomMargin: 0
            }
        },
        State {
            name: "hide"
            PropertyChanges {
                target: controls
                anchors.bottomMargin: -height
            }
        }
    ]

    transitions: Transition {
        PropertyAnimation {
            properties: "anchors.bottomMargin"
            duration: 200
        }
    }

    function show() {
        if (enabled && NSettings.onScreenControls && NSettings.onScreenControlsHideTime > 0) {
            state = "show"
            guiTimer.restart()
        }
    }

    Timer {
        id: guiTimer
        interval: NSettings.onScreenControlsHideTime
        running: false
        repeat: false
        onTriggered: {
            if (!mouseArea.containsMouse)
                state = "hide"
        }
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true
        preventStealing: true
        onExited: {
            guiTimer.restart()
        }
    }

    RowLayout {
        anchors.fill: parent
        anchors.leftMargin: 5
        anchors.rightMargin: 5
        Item {
            id: buttonPlay
            Layout.preferredHeight: 32
            Layout.preferredWidth: 32
            anchors.leftMargin: 100

            Image {
                id: buttonPlayImage
                anchors.fill: parent
                source: renderer.status === MpvObject.PS_Playing ? "icons/pause.png" : "icons/play.png"
            }

            MouseArea {
                anchors.fill: parent
                onClicked: renderer.togglePause()
            }
        }

        Slider {
            id: timeSlider
            Layout.fillWidth: true
            hoverEnabled: false
            from: 0
            to: 1
            value: {
                if (!pressed) {
                    renderer.playbackPositionNormalized
                } else {
                    0.0
                }
            }
            onValueChanged: {
                if (pressed)
                    renderer.setPlaybackPosition(value * renderer.playbackDuration)
            }
        }

        Text {
            id: positionText
            text: "%1/%2".arg(position).arg(duration)
            color: "white"

            property string position : "00:00:00"
            property string duration : "00:00:00"

            Connections {
                target: renderer
                onPlaybackPositionStringChanged: positionText.position = playbackPositionString
                onPlaybackDurationChanged: positionText.duration = renderer.formatTime(playbackDuration)
            }
        }
    }
}
