import QtWinExtras 1.0

import NPlayer 1.0

TaskbarButton {
    progress.visible: renderer.status > MpvObject.PS_Idle
    progress.paused: renderer.status & MpvObject.PS_Paused
    progress.value: renderer.playbackPositionNormalized * 100
}
