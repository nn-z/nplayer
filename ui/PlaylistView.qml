import QtQuick 2.11
import QtQuick.Controls 2.3
import QtQuick.Layouts 1.3

import NPlayer 1.0

Rectangle {
    id: rect
    width: 400
    height: parent.height
    color: "#cc1a1a1a"
    x: -width
    enabled: false

    onEnabledChanged: {
        if (!enabled) {
            historyView.setFocus()
        } else {
            view.focus = true
        }
    }

    function setFocus() {
        if (enabled) {
            view.focus = true
        }
    }

    states: [
        State {
            name: "show"
            when: enabled
            PropertyChanges {
                target: rect
                x: 0
            }
        },
        State {
            name: "hide"
            when: !enabled
            PropertyChanges {
                target: rect
                x: -width
            }
        }
    ]

    transitions: Transition {
        PropertyAnimation {
            properties: "x"
            duration: 200
        }
    }

    Shortcut {
        sequence: "Escape"
        onActivated: rect.enabled = false
        enabled: view.focus
    }

    MouseArea {
        anchors.fill: parent
        acceptedButtons: Qt.MiddleButton
        hoverEnabled: true
        onPositionChanged: {
            if (pressed) {
                var wantedWidth = Math.max(Math.min(mouseX, mainWindow.width), 400)
                rect.width = Math.min(wantedWidth, mainWindow.width / 2)
            }
        }
    }

    Text {
        text: "%1/%2/%3".arg(view.count === 0 ? 0 : Playlist.activeIndex + 1).arg(view.count === 0 ? 0 : view.currentIndex + 1).arg(view.count)
        color: "white"
    }

    Text {
        text: qsTr("Playlist")
        anchors.horizontalCenter: parent.horizontalCenter
        font.underline: true
        color: "white"
    }

    Text {
        text: "[%1]".arg(Playlist.shuffle ? "Z" : "-")
        anchors.right: parent.right
        anchors.rightMargin: 0
        Layout.alignment: Qt.AlignRight
        color: "white"
    }

    ListView {
        id: view
        anchors.fill: parent
        clip: true
        anchors.topMargin: 32
        currentIndex: 0

        ScrollBar.vertical: ScrollBar {}

        boundsBehavior: Flickable.DragOverBounds
        highlightMoveDuration : 0
        highlightResizeDuration: 0
        highlight: Rectangle {
            color: "gray"
        }

        model: Playlist

        Connections {
            target: Playlist
            onActiveIndexChanged: {
                view.currentIndex = Playlist.activeIndex
            }
        }

        delegate: Item {
            width: view.height
            height: itemFileName.height + itemFilePath.height

            Keys.onReturnPressed: {
                Playlist.activeIndex = index
            }

            Column {
                Text {
                    id: itemFileName
                    text: model.fileName
                    color: Playlist.activeIndex === index ? "red" : "white"
                }
                Text {
                    id: itemFilePath
                    text: model.filePath
                    color: "#666666"
                }
            }
            MouseArea {
                anchors.fill: parent
                acceptedButtons: Qt.LeftButton | Qt.RightButton
                onClicked: view.currentIndex = index
                onDoubleClicked: Playlist.activeIndex = index
            }
        }
    }
}
