import QtQuick 2.0
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

import NPlayer 1.0

Item {
    Connections {
        target: Updater
        onUpdateAvailableChanged: {
            if (Updater.updateAvailable) {
               popup.open()
                if (renderer.status === MpvObject.PS_Playing)
                    renderer.togglePause()
            }
        }
    }

    Popup {
        id: popup
        x: ((mainWindow.width + width) / 2) - width
        y: ((mainWindow.height + height) / 2) - height
        width: 300
        height: 150
        modal: true
        focus: true
        closePolicy: Popup.NoAutoClose

        onOpened: mainWindow.actionsEnabled = false
        onClosed: mainWindow.actionsEnabled = true

        Connections {
            target: Updater
            onUpdateAvailableChanged: {
                stackLayout.currentIndex = 1
            }
            onUpdateErrorMsgChanged: {
                stackLayout.currentIndex = 0
            }
            onUpdateRunningChanged: {
                stackLayout.currentIndex = 2
            }
        }

        StackLayout {
            id: stackLayout
            anchors.fill: parent
            currentIndex: 0

            Item {
                ColumnLayout {
                    anchors.fill: parent
                    Text {
                        text: Updater.updateErrorMsg
                        Layout.fillHeight: false
                        wrapMode: Text.WordWrap
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    }

                    Button {
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                        text: qsTr("Close")
                        onClicked: popup.close()
                    }
                }
            }

            Item {
                visible: !Updater.updateRunning
                ColumnLayout {
                    anchors.fill: parent
                    Text {
                        text: qsTr("Update available")
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                    }

                    RowLayout {
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                        Button {
                            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                            text: qsTr("Update")
                            onClicked: Updater.doUpdate()
                        }
                        Button {
                            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                            text: qsTr("Close")
                            onClicked: popup.close()
                        }
                    }
                }
            }

            Item {
                visible: Updater.updateRunning
                ColumnLayout {
                    anchors.fill: parent
                    Text {
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                        text: qsTr("Downloading...")
                    }

                    ProgressBar {
                        Layout.fillWidth: true
                        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
                        indeterminate: true
                    }
                }
            }
        }
    }
}
