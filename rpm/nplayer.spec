Name: nplayer
Version: 0.1
Release: 2%{?dist}
Summary: mpv frontend

License: GPLv3
URL: https://gitlab.com/nn-z/nplayer
Source0: https://gitlab.com/nn-z/nplayer/-/archive/release-%{version}/%{name}-release-%{version}.tar.gz

BuildRequires:	mpv-libs-devel
BuildRequires:	qt5-devel
BuildRequires:	gcc-c++
BuildRequires:	make

Requires:	mpv
Requires:	qt5

Recommends:     youtube-dl

Provides:	nplayer = %{version}-%{release}

%define debug_package %{nil}

%description
Frontend for mpv

%prep
%setup -q -n %{name}-release-%{version}
qmake-qt5

%build
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
INSTALL_ROOT=$RPM_BUILD_ROOT make install

cp -r %{_builddir}/%{name}-master/etc $RPM_BUILD_ROOT
desktop-file-validate $RPM_BUILD_ROOT/etc/%{name}.desktop

mkdir -p %{buildroot}%{_datadir}/applications
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/22x22/apps/  
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/24x24/apps/
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/36x36/apps/
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/64x64/apps/
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/72x72/apps/
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/96x96/apps/
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/128x128/apps/
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/192x192/apps/
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/256x256/apps/
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/512x512/apps/
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/1024x1024/apps/
mkdir -p %{buildroot}%{_datadir}/icons/hicolor/scalable/apps/

mv $RPM_BUILD_ROOT/etc/%{name}.desktop %{buildroot}%{_datadir}/applications
rsvg-convert -w 16 -h 16 $RPM_BUILD_ROOT/etc/nplayer.svg > %{buildroot}%{_datadir}/icons/hicolor/16x16/apps/nplayer.png
rsvg-convert -w 22 -h 22 $RPM_BUILD_ROOT/etc/nplayer.svg > %{buildroot}%{_datadir}/icons/hicolor/22x22/apps/nplayer.png
rsvg-convert -w 24 -h 24 $RPM_BUILD_ROOT/etc/nplayer.svg > %{buildroot}%{_datadir}/icons/hicolor/24x24/apps/nplayer.png
rsvg-convert -w 32 -h 32 $RPM_BUILD_ROOT/etc/nplayer.svg > %{buildroot}%{_datadir}/icons/hicolor/32x32/apps/nplayer.png
rsvg-convert -w 36 -h 36 $RPM_BUILD_ROOT/etc/nplayer.svg > %{buildroot}%{_datadir}/icons/hicolor/36x36/apps/nplayer.png
rsvg-convert -w 48 -h 48 $RPM_BUILD_ROOT/etc/nplayer.svg > %{buildroot}%{_datadir}/icons/hicolor/48x48/apps/nplayer.png
rsvg-convert -w 64 -h 64 $RPM_BUILD_ROOT/etc/nplayer.svg > %{buildroot}%{_datadir}/icons/hicolor/64x64/apps/nplayer.png
rsvg-convert -w 72 -h 72 $RPM_BUILD_ROOT/etc/nplayer.svg > %{buildroot}%{_datadir}/icons/hicolor/72x72/apps/nplayer.png
rsvg-convert -w 96 -h 96 $RPM_BUILD_ROOT/etc/nplayer.svg > %{buildroot}%{_datadir}/icons/hicolor/96x96/apps/nplayer.png
rsvg-convert -w 128 -h 128 $RPM_BUILD_ROOT/etc/nplayer.svg > %{buildroot}%{_datadir}/icons/hicolor/128x128/apps/nplayer.png
rsvg-convert -w 192 -h 192 $RPM_BUILD_ROOT/etc/nplayer.svg > %{buildroot}%{_datadir}/icons/hicolor/192x192/apps/nplayer.png
rsvg-convert -w 256 -h 256 $RPM_BUILD_ROOT/etc/nplayer.svg > %{buildroot}%{_datadir}/icons/hicolor/256x256/apps/nplayer.png
rsvg-convert -w 512 -h 512 $RPM_BUILD_ROOT/etc/nplayer.svg > %{buildroot}%{_datadir}/icons/hicolor/512x512/apps/nplayer.png
rsvg-convert -w 1024 -h 1024 $RPM_BUILD_ROOT/etc/nplayer.svg > %{buildroot}%{_datadir}/icons/hicolor/1024x1024/apps/nplayer.png
mv $RPM_BUILD_ROOT/etc/nplayer.svg %{buildroot}%{_datadir}/icons/hicolor/scalable/apps/nplayer.svg

# remove windows specific files
rm $RPM_BUILD_ROOT/etc/nplayer.rc
rm $RPM_BUILD_ROOT/etc/nplayer.ico

%clean
rm -rf $RPM_BUILD_ROOT

%files
%license LICENSE
%{_bindir}/*
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}*.*

%changelog
* Thu May 31 2018 Daniel Santonen <santonen.daniel@gmail.com> 1-2
- Add youtube-dl to recommends

* Mon May 21 2018 Daniel Santonen <santonen.daniel@gmail.com> 1-1
- Initial RPM release
