#include "updater.h"
#include "nsettings.h"
#include <QDebug>
#include <QStandardPaths>
#ifdef _WIN32
#include <windows.h>
#endif

Updater::Updater(QObject *parent) : QObject(parent)
{
#ifndef _WIN32
    Q_ASSERT_X(0, "Updater", "Do not use updater on linux");
#endif

    if (QSslSocket::sslLibraryVersionString().size() == 0) {
        qWarning() << "Updater: cannot download updates, unable to find ssl library";
        return;
    }

    m_urlMap.insert(Exe,     qMakePair(QUrl(QStringLiteral("https://gitlab.com/nn-z/nplayer/-/jobs/artifacts/master/raw/nplayer/nplayer.exe.md5?job=deploy-win")),
                                      QUrl(QStringLiteral("https://gitlab.com/nn-z/nplayer/-/jobs/artifacts/master/raw/nplayer/nplayer.exe?job=deploy-win"))));
    m_urlMap.insert(LibMpv,  qMakePair(QUrl(QStringLiteral("https://gitlab.com/nn-z/nplayer/-/jobs/artifacts/master/raw/nplayer/mpv-1.dll.md5?job=deploy-win")),
                                      QUrl(QStringLiteral("https://gitlab.com/nn-z/nplayer/-/jobs/artifacts/master/raw/nplayer/mpv-1.dll?job=deploy-win"))));

    // delete old update files if found
    deleteUpdateFiles();

    m_manager.setRedirectPolicy(QNetworkRequest::NoLessSafeRedirectPolicy);
    connect(&m_manager, &QNetworkAccessManager::finished, this, &Updater::downloadFinished);

    QTimer::singleShot(0, [=](){
        m_settings = qobject_cast<NSettings *>(nsetings_provider());
        checkForUpdate(m_settings->parser.update);
    });
}

void Updater::checkForUpdate(bool forceCheck)
{
    if (forceCheck) {
        downloadFile(m_urlMap.value(Exe).first);
    } else if (m_settings->updatesEnabled) {
        QDateTime lastCheck = m_settings->value(QStringLiteral("Update/lastCheck")).toDateTime();
        QDateTime currentDateTime = QDateTime::currentDateTimeUtc();
        if (lastCheck.isNull() || currentDateTime > lastCheck.addDays(1)) {
            m_settings->setValue(QStringLiteral("Update/lastCheck"), currentDateTime);
            downloadFile(m_urlMap.value(Exe).first);
        } else {
            qInfo() << "Updater, next check" << (lastCheck.addDays(1).toString(Qt::ISODate));
        }
    }
}

void Updater::doUpdate()
{
    downloadFile(m_urlMap.value(LibMpv).first);
    m_updateRunning = true;
    emit updateRunningChanged();
}

void Updater::downloadFinished(QNetworkReply *reply)
{
    Q_ASSERT(m_currentDownloads.removeOne(reply));

    QUrl url = reply->url();
    if (reply->error() != 0u) {
        m_updateErrorMsg = QStringLiteral("Unable to dowload update");
        emit updateErrorMsgChanged();
        qWarning() << reply->errorString();
        return;
    }

    const QByteArray data(reply->readAll());
    QString filename = saveFileName(url);
    saveToDisk(filename, data);

    const QString downloadChecksum = splitChecksum(data);

    if (compareUrl(url, m_urlMap.value(Exe).first)) {
        if (downloadChecksum != appChecksum()) {
            qInfo() << "new nplayer version available," << downloadChecksum;
            m_updateAvailable = true;
            emit updateAvailableChanged();
        }
    } else if (compareUrl(url, m_urlMap.value(LibMpv).first)) {
        if (downloadChecksum != libMpvChecksum()) {
            qInfo() << "new libMpv version available," << downloadChecksum;
            downloadFile(m_urlMap.value(LibMpv).second);
        } else {
            downloadFile(m_urlMap.value(Exe).second);
        }
    } else if (compareUrl(url, m_urlMap.value(LibMpv).second)) {
        updateFile(libMpvPath(), filename);
        downloadFile(m_urlMap.value(Exe).second);
    } else if (compareUrl(url, m_urlMap.value(Exe).second)) {
        qInfo() << "update downloaded";
        updateFile(QCoreApplication::applicationFilePath(), filename);
        if (QProcess::startDetached(QCoreApplication::applicationFilePath())) {
            qApp->quit();
        } else {
            qWarning() << "Updater, failed to start detached";
        }
    }
}

void Updater::downloadFile(const QUrl &url)
{
    QNetworkRequest request(url);
    request.setAttribute(QNetworkRequest::RedirectPolicyAttribute,
                         QNetworkRequest::NoLessSafeRedirectPolicy);
    QNetworkReply *reply = m_manager.get(request);
    m_currentDownloads.append(reply);
}

bool Updater::isHttpRedirect(QNetworkReply *reply) const
{
    int statusCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
    return statusCode == 301 || statusCode == 302 || statusCode == 303
            || statusCode == 305 || statusCode == 307 || statusCode == 308;
}

QString Updater::saveFileName(const QUrl &url)
{
    QString path = url.path();
    QString basename = QFileInfo(path).fileName();
    QString tmpLocation = QStandardPaths::writableLocation(QStandardPaths::TempLocation);
    return tmpLocation + "/" + basename;
}

void Updater::saveToDisk(const QString &filename, const QByteArray &data)
{
    QFile file(filename);
    if (!file.open(QIODevice::WriteOnly)) {
        qWarning() << "Updater, failed to save file: " << filename << ", " << file.errorString();
        return;
    }
    file.write(data);
    file.close();
}

bool Updater::compareUrl(const QUrl &a, const QUrl &b) const
{
    QString aname = QFileInfo(a.path()).fileName();
    QString bname = QFileInfo(b.path()).fileName();
    return aname == bname;
}

QString Updater::appChecksum() const
{
    QFile file(QCoreApplication::applicationFilePath());
    if (!file.open(QIODevice::ReadOnly)) {
        qWarning() << "Updater, unable get app checksum, " << file.errorString();
        return QString();
    }
    QByteArray data = file.readAll();
    return QString(QCryptographicHash::hash((data), QCryptographicHash::Md5).toHex());
}

QString Updater::libMpvChecksum() const
{
    QFile file(libMpvPath());
    if (!file.open(QIODevice::ReadOnly)) {
        qWarning() << "Updater, unable get libMpv checksum, " << file.errorString();
        return QString();
    }
    QByteArray data = file.readAll();
    return QString(QCryptographicHash::hash((data), QCryptographicHash::Md5).toHex());
}

QString Updater::splitChecksum(const QByteArray &data) const
{
    QString tmp(data);
    QStringList list = tmp.split(QStringLiteral(" "));
    return list.first();
}

QString Updater::libMpvPath() const
{
    return QCoreApplication::applicationDirPath() + "/mpv-1.dll";
}

void Updater::updateFile(const QString &file, const QString &newFile)
{
#ifdef _WIN32
    QString oldPath = file + ".old";
    if (!MoveFileEx((wchar_t*)file.utf16(), (wchar_t*)oldPath.utf16(), MOVEFILE_WRITE_THROUGH | MOVEFILE_REPLACE_EXISTING)) {
        qWarning() << "Updater, failed to move current file";
        return;
    }
    if (!MoveFileEx((wchar_t*)newFile.utf16(), (wchar_t*)file.utf16(), MOVEFILE_WRITE_THROUGH | MOVEFILE_REPLACE_EXISTING)) {
        qWarning() << "Updater, failed to move new file";
        return;
    }
#else
    Q_UNUSED(file);
    Q_UNUSED(newFile);
#endif
}

void Updater::deleteUpdateFiles()
{
#ifdef _WIN32
    QString oldAppPath = QCoreApplication::applicationFilePath() + ".old";
    DeleteFile((wchar_t*)oldAppPath.utf16());

    QString oldLibMpv = libMpvPath() + ".old";
    DeleteFile((wchar_t*)oldLibMpv.utf16());
#endif
}
