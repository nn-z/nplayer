#include <QCommandLineOption>
#include <QCommandLineParser>
#include <QGuiApplication>
#include <QLoggingCategory>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include <clocale>

#include "nguiapplication.h"

#include "actionhandler.h"
#include "history.h"
#include "mpvobject.h"
#include "nfiledialog.h"
#include "nsettings.h"
#include "playlist.h"

#ifdef _WIN32
#include <windows.h>
#include "updater.h"
#endif

void setCommandLineOptions(QCommandLineParser *parser)
{
    parser->setApplicationDescription(QStringLiteral("nplayer"));
    parser->addHelpOption();
    parser->addVersionOption();

    const QCommandLineOption fileOption(QStringList() << QStringLiteral("f") << QStringLiteral("file"),
                                        QCoreApplication::translate("file", "File to open."),
                                        QCoreApplication::translate("main", "file"));
    const QCommandLineOption folderOption(QStringList() << QStringLiteral("o") << QStringLiteral("folder"),
                                          QCoreApplication::translate("folder", "Folder to open."),
                                          QCoreApplication::translate("main", "folder"));
    const QCommandLineOption subtitleOption(QStringList() << QStringLiteral("s") << QStringLiteral("sub"),
                                            QCoreApplication::translate("sub", "Subtitle to add. (requires file option)"),
                                            QCoreApplication::translate("main", "file"));
#ifdef _WIN32
    const QCommandLineOption updateOption(QStringList() << "u" << "update",
                                          QCoreApplication::translate("update", "Check for update."));
#endif

    parser->addOption(fileOption);
    parser->addOption(folderOption);
    parser->addOption(subtitleOption);
#ifdef _WIN32
    parser->addOption(updateOption);
#endif
}

void setLogType()
{
    QSettings s;
    QString logTypeSetting(QStringLiteral("Settings/log/type"));
    if (s.contains(logTypeSetting)) {
        auto logType = qvariant_cast<MpvObject::LogType>(s.value(logTypeSetting));
        if (logType == MpvObject::LT_None) {
            QLoggingCategory::setFilterRules("*.debug=false\n"
                                             "*.critical=false\n"
                                             "*.warning=false\n"
                                             "*.info=false");
        }
    }
}

void setFontSize()
{
    QSettings s;
    bool customFont = s.value(QStringLiteral("Settings/ui/fontCustom"), false).toBool();
    if (customFont) {
        QString fontFamily = s.value(QStringLiteral("Settings/ui/fontFamily")).toString();
        if (fontFamily.isEmpty()) {
            return;
        }

        double fontSize = s.value(QStringLiteral("Settings/ui/fontSize"),
                                  QGuiApplication::font().pointSizeF()).toDouble();
        if (fontSize <= 0.0) {
            return;
        }

        QFont font(fontFamily);
        font.setPointSizeF(fontSize);
        QGuiApplication::setFont(font);
    }
}

int main(int argc, char *argv[])
{
#ifdef _WIN32
    if (AttachConsole(ATTACH_PARENT_PROCESS)) {
        freopen("CONOUT$", "w", stdout);
        freopen("CONOUT$", "w", stderr);
    }
#endif

    QCoreApplication::setOrganizationName(QStringLiteral("nn-z"));
    QCoreApplication::setApplicationName(QStringLiteral("nplayer"));
    QCoreApplication::setApplicationVersion(QStringLiteral("0.1"));
    //QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    setLogType();

    NGuiApplication app(argc, argv);

    if (!app.lock()) {
        qDebug() << "Instance is already running!";
        return  -100;
    }

    setFontSize();

    std::setlocale(LC_NUMERIC, "C");

    qmlRegisterType<MpvObject>("NPlayer", 1, 0, "MpvObject");
    qmlRegisterType<NFileDialog>("NPlayer", 1, 0, "PNFileDialog");
    qmlRegisterSingletonType<NSettings>("NPlayer", 1, 0, "NSettings", nsetings_provider);
    qmlRegisterSingletonType<ActionHandler>("NPlayer", 1, 0, "ActionHandler", actionHandler_provider);
    qmlRegisterSingletonType<Playlist>("NPlayer", 1, 0, "Playlist", playlist_provider);
    qmlRegisterSingletonType<History>("NPlayer", 1, 0, "History", history_provider);
#ifdef _WIN32
    qmlRegisterSingletonType<Updater>("NPlayer", 1, 0, "Updater", updater_provider);
#endif

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/PlayerWindow.qml")));
    if (engine.rootObjects().isEmpty()) {
        return -1;
    }

    QCommandLineParser parser;
    setCommandLineOptions(&parser);
    parser.process(app);

    auto settings = qobject_cast<NSettings *>(nsetings_provider());
    if (argc == 2) {
        settings->parser.file = QUrl::fromLocalFile(QString(argv[1]));
    } else if (argc > 2) {
        settings->parser.file = QUrl::fromLocalFile(parser.value(QStringLiteral("file")));
        settings->parser.folder = QUrl::fromLocalFile(parser.value(QStringLiteral("folder")));
        settings->parser.subtitle = QUrl::fromLocalFile(parser.value(QStringLiteral("sub")));
#ifdef _WIN32
        settings->parser.update = parser.isSet(QStringLiteral("update"));
#endif
    }

    return NGuiApplication::exec();
}
