#include "include/nguiapplication.h"

NGuiApplication::NGuiApplication(int &argc, char **argv) : QGuiApplication(argc, argv)
{
    m_instance = QSharedPointer<QSharedMemory>::create(QStringLiteral("NGuiApplicationInstance"), this);
}

NGuiApplication::~NGuiApplication()
{
    if(m_instance->isAttached()) {
        m_instance->detach();
    }
}

bool NGuiApplication::lock() const
{
    if(m_instance->attach(QSharedMemory::ReadOnly)){
        m_instance->detach();
        return false;
    }

    return m_instance->create(1);
}
