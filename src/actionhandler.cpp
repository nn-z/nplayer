#include "actionhandler.h"

#include <QDebug>
#include <QTimer>

ActionHandler::ActionHandler(QObject *parent) : QObject(parent)
{
    m_settings = QSharedPointer<QSettings>::create();

    m_settings->beginGroup(QStringLiteral("Shortcuts"));
    load(QStringLiteral("openFile"),                { "Ctrl+F" });
    load(QStringLiteral("openFolder"),              { "Ctrl+O" });
    load(QStringLiteral("openURL"),                 { "Shift+O" });
    load(QStringLiteral("shuffle"),                 { "Z" });
    load(QStringLiteral("playToggle"),              { "Space" });
    load(QStringLiteral("prevItem"),                { "P", "." });
    load(QStringLiteral("nextItem"),                { "N", "/" });
    load(QStringLiteral("seek+short"),              { "Right" });
    load(QStringLiteral("seek-short"),              { "Left" });
    load(QStringLiteral("seek+medium"),             { "Ctrl+Right" });
    load(QStringLiteral("seek-medium"),             { "Ctrl+Left" });
    load(QStringLiteral("seek+long"),               { "Shift+Right" });
    load(QStringLiteral("seek-long"),               { "Shift+Left" });
    load(QStringLiteral("clearPlaylist"),           { "Shift+C" });
    load(QStringLiteral("fullscreen"),              { "F" });
    load(QStringLiteral("scale100%"),               { "1" });
    load(QStringLiteral("scale125%"),               { "2" });
    load(QStringLiteral("scale150%"),               { "3" });
    load(QStringLiteral("scale200%"),               { "4" });
    load(QStringLiteral("volumeStepUp"),            { "Up" });
    load(QStringLiteral("volumeStepDown"),          { "Down" });
    load(QStringLiteral("volumeMute"),              { "M" });
    load(QStringLiteral("showConfig"),              { "Ctrl+C" });
    load(QStringLiteral("showPlaylist"),            { "L" });
    load(QStringLiteral("showHistory"),             { "H" });
    load(QStringLiteral("showOnScreenDisplay"),     { "Tab" });
    load(QStringLiteral("showOnScreenControls"),    { "Ctrl+M" });
    load(QStringLiteral("showonScreenSeekBar"),     { "" });
    load(QStringLiteral("showOnScreenHint"),        { "" });
    load(QStringLiteral("quit"),                    { "Ctrl+Q" });
    load(QStringLiteral("addSubtitle"),             { "" });
    load(QStringLiteral("removeSubtitle"),          { "" });
    load(QStringLiteral("showSubtitles"),           { "V" });
    load(QStringLiteral("nextSubtitleTrack"),       { "S" });
    load(QStringLiteral("nextAudioTrack"),          { "A" });
    load(QStringLiteral("showShortcutMap"),         { "F1" });
    load(QStringLiteral("clearHistory"),            { "" });
    load(QStringLiteral("streamQualtyScreen"),      { "" });
    load(QStringLiteral("streamQualty480"),         { "" });
    load(QStringLiteral("streamQualty720"),         { "" });
    load(QStringLiteral("streamQualty1080"),        { "" });
    load(QStringLiteral("streamQualty1440"),        { "" });
    load(QStringLiteral("streamQualty2160"),        { "" });
    load(QStringLiteral("debugProperty"),           { "`" });
    load(QStringLiteral("volumeNormalize"),         { "Shift+N" });
    load(QStringLiteral("subOverrideStyle"),        { "" });
    m_settings->endGroup();

    QTimer::singleShot(0, [=](){
        emit mapChanged();
    });
}

void ActionHandler::setShortcut(ActionType type, int index, const QString& shortcut)
{
    QStringList shortcuts =  m_actions.at(index).shortcuts;
    if (shortcuts.size() == 2 || type == Primary) {
        shortcuts[type] = shortcut;
    } else {
        shortcuts.append(shortcut);
    }

    m_actions[index].shortcuts = shortcuts;
}

QString ActionHandler::shortcut(ActionHandler::ActionType type, const QString& action)
{
    const int index = m_actions.indexOf(Action{action, QStringList()});
    return (index < 0) ? QString() : shortcutByIndex(type, index);
}

QString ActionHandler::shortcutByIndex(ActionType type, int index)
{
    QStringList shortcuts = m_actions.at(index).shortcuts;
    return type == Primary ? shortcuts.first() :
                             shortcuts.size() == 2 ? shortcuts.last() : QString();
}

QString ActionHandler::actionByIndex(int index)
{
    return m_actions.at(index).action;
}

int ActionHandler::num() const
{
    return m_actions.size();
}

bool ActionHandler::containsShortcut(const QString& shortcut)
{
    return m_cachedShortcuts.contains(shortcut, Qt::CaseInsensitive);
}

void ActionHandler::save()
{
    m_cachedShortcuts.clear();

    m_settings->beginGroup(QStringLiteral("Shortcuts"));
    for (const Action &action : qAsConst(m_actions)) {
        m_cachedShortcuts.append(action.shortcuts);
        m_settings->setValue(action.action, action.shortcuts);
    }
    m_settings->endGroup();

    emit mapChanged();
}

void ActionHandler::reset()
{
    m_settings->beginGroup(QStringLiteral("Shortcuts"));
    m_settings->remove(QString());
    m_settings->endGroup();
}

void ActionHandler::load(const QString& action, const QStringList& defaultShortcuts)
{
    Q_ASSERT(defaultShortcuts.size() <= 2);

    m_cachedShortcuts.append(defaultShortcuts);

    QVariant value = m_settings->value(action, defaultShortcuts);
    m_actions.append(Action{action, value.toStringList()});
}
