#include "history.h"
#include <QDebug>
#include <QStandardPaths>
#include <QTimer>

#include "nsettings.h"

History::History(QObject *parent) : QAbstractListModel(parent)
{
    QTimer::singleShot(0, [=](){
        m_settings = qobject_cast<NSettings *>(nsetings_provider());
        loadHistory();
    });
}

int History::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()) {
        return 0;
    }

    return m_items.size();
}

QVariant History::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    const HistoryItem item = m_items.at(index.row());
    switch (role) {
    case FileNameRole:
        return item.url.fileName();
    case FilePathRole:
        return item.url.toLocalFile();
    case FileDateRole:
        return QStringLiteral("[%1] ").arg(item.date.toString(QStringLiteral("MMM dd")));
    case FileExists:
        return item.exists;
    case ModelEnabled:
        return item.enabled;
    }
    return QVariant();
}

QHash<int, QByteArray> History::roleNames() const
{
    QHash<int, QByteArray> names;
    names[FileNameRole] = "fileName";
    names[FilePathRole] = "filePath";
    names[FileDateRole] = "fileDate";
    names[FileExists] = "fileExists";
    names[ModelEnabled] = "modelEnabled";
    return names;
}

void History::save()
{
    if (!m_settings->historyEnabled) {
        return;
    }

    QDir dir;
    if (!dir.exists(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation))) {
        dir.mkpath(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation));
    }

    QFile file(historyFilePath());
    if (!file.open(QIODevice::ReadWrite | QIODevice::Text)) {
        qWarning() << "History, unable to write history file";
        return;
    }

    file.resize(0);
    QTextStream stream(&file);
    for (const HistoryItem &pair : qAsConst(m_items)) {
        if (pair.url.isValid()) {
            stream << pair.url.toString() << "="
                   << QString::number(pair.time) << "="
                   << pair.date.toString() << "\n";
        }
        else {
            qWarning() << "History, found invalid url during save";
        }
    }
}

double History::fileTime(const QUrl &url) const
{
    const int index = m_items.indexOf(HistoryItem(url));
    return index == -1 ? 0.0 : m_items.at(index).time;
}

void History::pushFileToFront(const QUrl &url, double time)
{
    if (!url.isValid()) {
        qWarning() << "History, pushFileToFront(), invalid url";
        return;
    }

    HistoryItem historyItem(url, time);
    int urlIndex = m_items.indexOf(historyItem);
    if (urlIndex != -1) {
        // update time
        m_items[urlIndex].time = time;

        // remove current row
        beginRemoveRows(QModelIndex(), urlIndex, urlIndex);
        endRemoveRows();

        // add new row to front
        beginInsertRows(QModelIndex(), 0, 0);
        m_items.move(urlIndex, 0);
        endInsertRows();
    } else {
        if ((m_settings->historyMaxSize != -1) && (m_items.size() == m_settings->historyMaxSize)) {
            beginRemoveRows(QModelIndex(), m_items.size() - 1, m_items.size() - 1);
            endRemoveRows();
            m_items.removeLast();
        }
        // file is new to history
        beginInsertRows(QModelIndex(), 0, 0);
        m_items.push_front(historyItem);
        endInsertRows();
    }
}

void History::clear()
{
    QFile::remove(historyFilePath());
    clearItems();
}

void History::filter(const QString &text)
{
    if (text.isEmpty()) {
        return;
    }

    auto tmp = m_items;
    clearItems();

    m_filterActive = true;
    emit filterActiveChanged(m_filterActive);

    for (HistoryItem &item : tmp) {
        preInsertItem();
        item.enabled = item.url.toString().contains(text, Qt::CaseInsensitive);
        m_items.append(item);
        postInsertItem();
    }
}

void History::clearFilter()
{
    auto tmp = m_items;
    clearItems();

    m_filterActive = false;
    emit filterActiveChanged(m_filterActive);

    for (HistoryItem &item : tmp) {
        preInsertItem();
        item.enabled = true;
        m_items.append(item);
        postInsertItem();
    }
}

void History::preInsertItem()
{
    const int index = m_items.size();
    beginInsertRows(QModelIndex(), index, index);
}

void History::postInsertItem()
{
    endInsertRows();
}

void History::clearItems()
{
    const int index = m_items.size() + 1;
    beginRemoveRows(QModelIndex(), 0, index);
    endRemoveRows();

    m_items.clear();
}

QUrl History::urlAt(int index) const
{
    return m_items.at(index).url;
}

HistoryItem History::at(int index) const
{
    return m_items.at(index);
}

QString History::historyFilePath() const
{
    return QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation) + "/history";
}

void History::loadHistory()
{
    if (!m_settings->historyEnabled) {
        return;
    }

    QFile historyFile(historyFilePath());
    if (!historyFile.exists()) {
        return;
    }

    if(!historyFile.open(QIODevice::ReadOnly)) {
        qWarning() << "History, unable to read history file";
    }

    QTextStream in(&historyFile);
    while(!in.atEnd()) {
        QString line = in.readLine();
        if ((m_settings->historyMaxSize != -1) && (m_items.size() == m_settings->historyMaxSize)) {
            break;
        }

        QStringList list = line.split(QStringLiteral("="));
        if (list.size() == 3) {
            QUrl file(list.first());
            double time = list.at(1).toDouble();
            QDate date = QDate::fromString(list.at(2));

            QFileInfo fi(file.toLocalFile());
            bool exists = fi.exists() && fi.isFile();

            if (!file.isValid()) {
                qWarning() << "History, invalid file";
            } else {
                preInsertItem();
                m_items.append(HistoryItem(file, time, date, exists));
                postInsertItem();
            }
        } else {
            qWarning() << "History, param count mismatch," << list.size();
        }
    }
}
