#include "nsettings.h"

#include <QGuiApplication>

NSettings::NSettings(QObject *parent) : QSettings(parent)
{
    beginGroup(QStringLiteral("Settings"));
    add<bool>(&playlistPopulateFromSubdirectories, false, QStringLiteral("playlist/populateFromSubdirectories"));
    add<bool>(&playlistRepopulateOnDrag, true, QStringLiteral("playlist/repopulateOnDrag"));
    add<bool>(&hwDecoding, true, QStringLiteral("video/hwDecoding"));
    add<bool>(&autoVideoResize, true, QStringLiteral("video/autoVideoResize"));
    add<QString>(&audioDefaultTrack, QString(), QStringLiteral("audio/audioDefaultTrack"));
    add<QString>(&subDefaultTrack, QString(), QStringLiteral("sub/subDefaultTrack"));
    add<int>(&onScreenControlsHideTime, 1000, QStringLiteral("general/onScreenControlsHideTime"));
    add<int>(&onScreenHintHideTime, 500, QStringLiteral("general/onScreenHintHideTime"));
    add<int>(&onScreenSeekBarHideTime, 500, QStringLiteral("general/onScreenSeekBarHideTime"));
    add<int>(&cursorHideTime, 1000, QStringLiteral("general/cursorHideTime"));
    add<double>(&onScreenSeekBarOpacity, 0.8, QStringLiteral("general/onScreenSeekBarOpacity"));
    add<bool>(&menuItemShowBothShortcuts, false, QStringLiteral("menu/itemShowBothShortcuts"));
    add<bool>(&historyEnabled, true, QStringLiteral("history/enabled"));
    add<int>(&historyMaxSize, -1, QStringLiteral("history/maxSize"));
    add<double>(&seekShort, 5.0, QStringLiteral("video/seekShort"));
    add<double>(&seekMedium, 10.0, QStringLiteral("video/seekMedium"));
    add<double>(&seekLong, 30.0, QStringLiteral("video/seekLong"));
    add<double>(&volumeStep, 5.0, QStringLiteral("audio/volumeStep"));
    add<bool>(&updatesEnabled, true, QStringLiteral("updates/enabled"));
    add<bool>(&updatesUseGit, true, QStringLiteral("updates/git"));
    add<MpvObject::LogType>(&logType, MpvObject::LogType::LT_Console, QStringLiteral("log/type"));
    add<MpvObject::MpvMsgLevel>(&mpvMsgLevel, MpvObject::MML_Fatal, QStringLiteral("log/mpvMsgLevel"));
    add<MpvObject::SubAutoLoad>(&subAutoLoad, MpvObject::SAL_Exact, QStringLiteral("sub/autoLoad"));
    add<bool>(&uiFontCustom, false, QStringLiteral("ui/fontCustom"));
    add<QString>(&uiFontFamily, QString(), QStringLiteral("ui/fontFamily"));
    add<double>(&uiFontSize, 10.0, QStringLiteral("ui/fontSize"));
    add<bool>(&uiNativeFileDialog, false, QStringLiteral("ui/nativeFileDialog"));
    add<bool>(&playlistFileSystemWatch, false, QStringLiteral("playlist/fileSystemWatch"));

    // non persistent properties
    add<bool>(&onScreenSeekBar, true, QString());
    add<bool>(&onScreenControls, true, QString());
    add<bool>(&onScreenHint, true, QString());
    endGroup();

    QTimer::singleShot(0, [=](){
        emit hwDecodingChanged();
        emit subAutoLoadChanged();
        emit mpvMsgLevelChanged();
        emit playlistFileSystemWatchChanged();
    });
}

void NSettings::save()
{
    beginGroup(QStringLiteral("Settings"));
    for (const QSharedPointer<AbstractSetting> &setting : qAsConst(m_abstractSettings)) {
        setting->save(this);
    }
    endGroup();
}

void NSettings::resetSettings()
{
    beginGroup(QStringLiteral("Settings"));
    remove(QString());
    endGroup();
}
