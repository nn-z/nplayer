#include "playlist.h"
#include <QCollator>
#include <QDebug>
#include <QDir>
#include <QDirIterator>
#include <QFileInfo>
#include <QRandomGenerator>
#include <QTimer>
#include <QVariant>

#include <cmath>
#include <cstdlib>
#include <ctime>

#include "mpvobject.h"
#include "nsettings.h"

Playlist::Playlist(QObject *parent) : QAbstractListModel(parent)
{
    const QStringList suffixes = MpvObject::videoSuffixes();
    m_videoSuffixes.reserve(suffixes.size());
    for (const QString &s : qAsConst(suffixes)) {
        m_videoSuffixes.append("*." + s);
    }

    QTimer::singleShot(0, [=](){
        m_settings = qobject_cast<NSettings *>(nsetings_provider());
        connect(m_settings, &NSettings::playlistFileSystemWatchChanged, this, &Playlist::fileSystemWatchChanged);
        connect(m_settings, &NSettings::playlistPopulateFromSubdirectoriesChanged, this, &Playlist::fileSystemWatchChanged);
    });
}

int Playlist::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid()) {
        return 0;
    }

    return items().size();
}

QVariant Playlist::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()) {
        return QVariant();
    }

    const QUrl item = m_items.at(index.row());
    switch (role) {
    case FileNameRole:
        return item.fileName();
    case FilePathRole:
        return item.toLocalFile();
    }

    return QVariant();
}

QHash<int, QByteArray> Playlist::roleNames() const
{
    QHash<int, QByteArray> names;
    names[FileNameRole] = "fileName";
    names[FilePathRole] = "filePath";
    return names;
}

bool Playlist::setItemAt(int index, const QUrl &item)
{
    if (!validIndex(index)) {
        return false;
    }

    const QUrl &oldItem = m_items.at(index);
    if (item.toLocalFile() == oldItem.toLocalFile()) {
        return false;
    }

    m_items[index] = item;
    return true;
}

void Playlist::clearPlaylist()
{
    if (!m_items.empty()) {
        const int index = items().size() + 1;
        beginRemoveRows(QModelIndex(), 0, index);
        endRemoveRows();

        m_items.clear();

        m_activeIndex = 0;
        emit activeIndexChanged();
    }
}

bool Playlist::nextFile(bool ignoreIncrement)
{
    bool result = false;
    if (m_shuffle) {
        shuffleFile();
        result = true;
    } else if (m_activeIndex + 1 < m_items.size()) {
        if (!ignoreIncrement) {
            m_activeIndex++;
        }
        emit activeIndexChanged();
        emit loadFile(m_items[m_activeIndex]);
        result = true;
    }
    return result;
}

bool Playlist::prevFile()
{
    bool result = false;
    if (m_shuffle) {
        shuffleFile();
        result = true;
    } else if (m_activeIndex - 1 >= 0) {
        m_activeIndex--;
        emit activeIndexChanged();
        emit loadFile(m_items[m_activeIndex]);
        result = true;
    }
    return result;
}

void Playlist::filesDropped(const QList<QUrl> &files)
{
    if (files.isEmpty()) {
        return;
    }

    if (m_settings->playlistRepopulateOnDrag) {
        populateFromFile(files.first().toString());
    } else {
        addItems(files, true);
    }

    emit loadFile(files.first());
}

void Playlist::addItem(const QUrl &file, bool setIndex)
{
    if (containsFile(file)) {
        return;
    }

    preInsertItem();
    m_items.append(file);
    postInsertItem();

    if (setIndex) {
        m_activeIndex = indexOf(file);
        emit activeIndexChanged();
    }
}

void Playlist::addItems(const QList<QUrl> &files, bool setIndex)
{
    if (files.isEmpty()) {
        return;
    }

    for (const QUrl &url : qAsConst(files)) {
        if (containsFile(url)) {
            continue;
        }

        preInsertItem();
        m_items.append(url);
        postInsertItem();
    }

    if (setIndex) {
        m_activeIndex = indexOf(files.first());
        emit activeIndexChanged();
    }
}

void Playlist::fileSystemWatchChanged()
{
    if (m_settings->playlistFileSystemWatch && !m_settings->playlistPopulateFromSubdirectories) {
        m_fsWatcher = QSharedPointer<QFileSystemWatcher>::create();
        connect(m_fsWatcher.data(), &QFileSystemWatcher::directoryChanged, this, &Playlist::directoryChanged);
        if (!m_currentDir.isEmpty()) {
            m_fsWatcher->addPath(m_currentDir.absolutePath());
        }
    } else {
        m_fsWatcher.clear();
    }
}

void Playlist::directoryChanged(const QString &path)
{
    populate(QDir(path), m_items.at(m_activeIndex), false);
}

bool Playlist::validIndex(int index)
{
    return index >= 0 && index <= m_items.size();
}

void Playlist::preInsertItem()
{
    const int index = items().size();
    beginInsertRows(QModelIndex(), index, index);
}

void Playlist::postInsertItem()
{
    endInsertRows();
}

void Playlist::populate(QDir dir, const QUrl& targetUrl, bool autoLoad)
{
    m_currentDir = dir;

    clearPlaylist();

    dir.setNameFilters(m_videoSuffixes);
    dir.setFilter(QDir::Files | QDir::NoSymLinks | QDir::NoDot | QDir::NoDotDot);

    QVector<QUrl> tmpItems;

    auto localFiletoUrl = [dir](const QString &file)
    {
        return QUrl::fromLocalFile(dir.absoluteFilePath(file));
    };

    if (m_settings->playlistPopulateFromSubdirectories) {
        QDirIterator it(dir, QDirIterator::Subdirectories);
        while (it.hasNext()) {
            tmpItems.append(localFiletoUrl(it.next()));
        }
    } else {
        QStringList entryList = dir.entryList();

        if (entryList.isEmpty()) {
            qWarning() << "Playlist: directory is empty, " << dir;
            return;
        }

        tmpItems.reserve(entryList.size());
        for (const QString &entry : qAsConst(entryList)) {
            tmpItems.append(localFiletoUrl(entry));
        }
    }

    QCollator collator;
    collator.setNumericMode(true);
    collator.setCaseSensitivity(Qt::CaseSensitive);
    collator.setIgnorePunctuation(true);

    std::sort(tmpItems.begin(),
              tmpItems.end(),
              [&collator](const QUrl &a, const QUrl &b)
    {
        return collator.compare(a.fileName(), b.fileName()) < 0;
    });

    int i = -1;
    for (const QUrl &playlistItem : qAsConst(tmpItems)) {
        preInsertItem();
        m_items.append(playlistItem);
        postInsertItem();

        if (!targetUrl.isEmpty()) {
            if (playlistItem.toLocalFile() == targetUrl.toLocalFile()) {
                m_activeIndex = i + 1;
                emit activeIndexChanged();
            }
            i++;
        }
    }

    if (autoLoad) {
        // set path to watch
        if (!m_settings->playlistPopulateFromSubdirectories && !m_fsWatcher.isNull()) {
            m_fsWatcher->removePaths(m_fsWatcher->directories());
            m_fsWatcher->addPath(dir.absolutePath());
        }

        if (!targetUrl.isEmpty()) {
            if (i < 0) {
                qWarning() << "Playlist: unable to set active index";
            }
            else {
                emit loadFile(targetUrl);
            }
        } else if (!m_items.empty()) {
            emit loadFile(m_items.constFirst());
        }
    }
}

void Playlist::setActiveIndex(int index)
{
    if (index < 0 || index > m_items.size()) {
        return;
    }

    m_activeIndex = index;
    emit activeIndexChanged();

    emit loadFile(m_items[index]);
}

void Playlist::setShuffle(bool val)
{
    m_shuffle = val;
    emit shuffleChanged();
}

void Playlist::populateFromFile(const QUrl &item)
{
    populate(QDir(QFileInfo(item.toLocalFile()).absoluteDir()), item);
}

void Playlist::populateFromFolder(const QUrl &item)
{
    QString path = item.toLocalFile();
    QFileInfo fi(path);
    if (!fi.isDir()) {
        path = fi.path();
    }

    populate(QDir(path));
}

QString Playlist::fileNameAt(int index) const
{
    return m_items[index].fileName();
}

QString Playlist::filePathAt(int index) const
{
    return m_items[index].toLocalFile();
}

int Playlist::randomIndex() const
{
    if (m_items.size() <= 1) {
        return -1;
    }

    int randIndex = m_activeIndex;
    while (randIndex == m_activeIndex) {
        auto randInt = static_cast<int>(QRandomGenerator::global()->generate());
        randIndex = randInt % m_items.size();
    }
    Q_ASSERT(randIndex >= 0 && randIndex < m_items.size());
    return randIndex;
}

int Playlist::indexOf(const QUrl &file) const
{
    int i = 0;
    for (const QUrl &playlistItem : qAsConst(m_items)) {
        if (playlistItem.toLocalFile() == file.toLocalFile()) {
            return i;
        }
        i++;
    }
    return i;
}

bool Playlist::containsFile(const QUrl &file)
{    
    for (const QUrl &playlistItem : qAsConst(m_items)) {
        if (playlistItem.toLocalFile() == file.toLocalFile()) {
            return true;
        }
    }
    return false;
}

void Playlist::shuffleFile()
{
    const int randIndex = randomIndex();
    m_activeIndex = randIndex;
    emit activeIndexChanged();
    emit loadFile(m_items[m_activeIndex]);
}
