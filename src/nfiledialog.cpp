#include "nfiledialog.h"

#include "mpvobject.h"

#include <QDebug>
#include <QDir>
#include <QSettings>
#include <QStandardPaths>

NFileDialog::NFileDialog(QWindow *parent) : QQuickWindow(parent)
{
    QSettings s;
    m_userPaths = s.value(QStringLiteral("NFileDialog/items")).toStringList();

    m_hidden = s.value(QStringLiteral("NFileDialog/hidden"), false).toBool();

    m_systemPaths.append(QDir::homePath());
    QFileInfoList drives = QDir::drives();
    m_systemPaths.reserve(drives.size() + 1);
    for (const QFileInfo& fi : qAsConst(drives)) {
        m_systemPaths.append(fi.filePath());
    }

    QUrl recentFile = s.value(QStringLiteral("Recent/lastFile")).toUrl();
    if (recentFile.isValid()) {
        m_path = QDir(recentFile.toLocalFile());
        m_path.cdUp();
    } else {
        m_path = QDir::home();
    }

    m_path.setSorting(QDir::DirsFirst | QDir::IgnoreCase);

    setFilter(NFileDialog::VideoFiles);
}

void NFileDialog::parentDir()
{
    if (m_path.cdUp()) {
        emit pathChanged(m_path);
        processDir();
    }
}

void NFileDialog::relativeDir(const QString& dir)
{
    QFileInfo fi(m_path, dir);
    if (fi.isDir()) {
        m_path.setPath(m_path.absolutePath() + "/" + dir);
        emit pathChanged(m_path);
        processDir();
    }
}

void NFileDialog::absoluteDir(const QString &dir)
{
    QFileInfo fi(dir);
    if (fi.exists() && fi.isDir()) {
        m_path.setPath(dir);
        emit pathChanged(m_path);
        processDir();
    }
}

QString NFileDialog::pathString() const
{
    return m_path.absolutePath();
}

void NFileDialog::addUserPath(int index)
{
    QString item = m_path.absoluteFilePath(m_files.at(index));
    if (m_userPaths.contains(item)) {
        return;
    }

    m_userPaths.append(item);
    emit userPathsChanged(m_userPaths);

    QSettings s;
    s.setValue(QStringLiteral("NFileDialog/items"), m_userPaths);
}

void NFileDialog::removeUserPath(int index)
{
    QString item = m_userPaths.at(index);
    if (!m_userPaths.contains(item)) {
        return;
    }

    m_userPaths.removeAll(item);
    emit userPathsChanged(m_userPaths);

    QSettings s;
    s.setValue(QStringLiteral("NFileDialog/items"), m_userPaths);
}

QString NFileDialog::userPathString(int index) const
{
    QFileInfo fi(m_userPaths.at(index));
    return fi.fileName();
}

QString NFileDialog::systemPathString(int index) const
{
    QDir dir(m_systemPaths.at(index));
    QFileInfo fi(dir, QString());
    if (QDir::drives().contains(fi)) {
        return dir.absolutePath();
    }
    return dir.dirName();
}

bool NFileDialog::isFile(const QString &relativeFile)
{
    QFileInfo fi(m_path, relativeFile);
    return fi.exists() && fi.isFile();
}

QString NFileDialog::itemType(int index) const
{
    QString file = m_files.at(index);
    if (file == QStringLiteral("..")) {
        return QString();
    }

    QFileInfo fi(m_path, file);
    return fi.isDir() ? QStringLiteral("folder") : fi.suffix();
}

QUrl NFileDialog::convertToUrl(const QString &relativeFile) const
{
    return QUrl::fromLocalFile(m_path.absoluteFilePath(relativeFile));
}

void NFileDialog::setFilter(NFileDialog::FilterType filter)
{
    m_filter = filter;
    emit filterChanged(m_filter);

    QFlags<QDir::Filter> filters(QDir::Dirs | QDir::NoSymLinks | QDir::NoDot | QDir::NoDotDot);
    filters.setFlag(QDir::Files, filter != NFileDialog::Folders);
    filters.setFlag(QDir::Hidden, m_hidden);
    m_path.setFilter(filters);
    processDir();
}

void NFileDialog::setHidden(bool hidden)
{
    m_hidden = hidden;
    emit hiddenChanged(m_hidden);

    QSettings s;
    s.setValue(QStringLiteral("NFileDialog/hidden"), hidden);

    QFlags<QDir::Filter> filters = m_path.filter();
    filters.setFlag(QDir::Hidden, hidden);
    m_path.setFilter(filters);
    processDir();
}

void NFileDialog::processDir()
{
    QStringList entryList = m_path.entryList();

    m_files.clear();
#ifdef __linux__
    // do not add ".." for root path
    if (m_path.absolutePath() != QStringLiteral("/")) {
        m_files.append(QStringLiteral(".."));
    }
#else
    m_files.append(QStringLiteral(".."));
#endif
    for (const QString &entry : qAsConst(entryList)) {
        QFileInfo fi(m_path.absoluteFilePath(entry));
        if (fi.isFile()) {
            if (m_filter == NFileDialog::VideoFiles &&
                    !MpvObject::videoSuffixes().contains(fi.suffix())) {
                continue;
            }
            if (m_filter == NFileDialog::SubtitleFiles &&
                    !MpvObject::subtitleSuffixes().contains(fi.suffix())) {
                continue;
            }
        }

        m_files.append(m_path.relativeFilePath(entry));
    }

    emit filesChanged(m_files);
}
