#include "mpvobject.h"

#include <QDateTime>
#include <QDir>
#include <QFileInfo>
#include <QGuiApplication>
#include <QOpenGLContext>
#include <QOpenGLFramebufferObject>
#include <QQuickWindow>
#include <QScreen>

#include <clocale>
#include <sstream>

#include "history.h"
#include "nsettings.h"
#include "playlist.h"

void onMpvEvents(void *ctx)
{
    QMetaObject::invokeMethod(static_cast<MpvObject*>(ctx), "onMpvEvents", Qt::QueuedConnection);
}

void onMpvRedraw(void *ctx)
{
    MpvObject::onUpdateStatic(ctx);
}

static void *getProcAddressMpv(void *ctx, const char *name)
{
    Q_UNUSED(ctx)

    QOpenGLContext *glctx = QOpenGLContext::currentContext();
    if (glctx == nullptr) {
        return nullptr;
    }

    return reinterpret_cast<void *>(glctx->getProcAddress(QByteArray(name)));
}

/***
 * MpvRenderer
 */
class MpvRenderer : public QQuickFramebufferObject::Renderer
{
private:
    Q_DISABLE_COPY(MpvRenderer)
    MpvObject *m_obj;

public:
    explicit MpvRenderer(MpvObject *obj)
        : m_obj(obj)
    {
        mpv_set_wakeup_callback(m_obj->m_mpv, onMpvEvents, m_obj);
    }

    ~MpvRenderer() override = default;

    QOpenGLFramebufferObject* createFramebufferObject(const QSize &size) override
    {
        if (m_obj->m_mpvContext == nullptr)
        {
            mpv_opengl_init_params gl_init_params{getProcAddressMpv, nullptr, nullptr};
            mpv_render_param params[]{
                { MPV_RENDER_PARAM_API_TYPE, const_cast<char *>(MPV_RENDER_API_TYPE_OPENGL) },
                { MPV_RENDER_PARAM_OPENGL_INIT_PARAMS, &gl_init_params },
                { MPV_RENDER_PARAM_INVALID, nullptr }
            };

            int err = mpv_render_context_create(&m_obj->m_mpvContext, m_obj->m_mpv, params);
            Q_ASSERT_X(err >= 0, "MpvRenderer", "failed to initialize mpv GL context");
            mpv_render_context_set_update_callback(m_obj->m_mpvContext, onMpvRedraw, m_obj);

            m_obj->rendererCreated();
        }

        return QQuickFramebufferObject::Renderer::createFramebufferObject(size);
    }

    void render() override
    {
        m_obj->window()->resetOpenGLState();

        QOpenGLFramebufferObject *fbo = framebufferObject();
        mpv_opengl_fbo mpfbo{static_cast<int>(fbo->handle()), fbo->width(), fbo->height(), 0};
        int flip_y{0};

        mpv_render_param params[] = {
            { MPV_RENDER_PARAM_OPENGL_FBO, &mpfbo },
            { MPV_RENDER_PARAM_FLIP_Y, &flip_y },
            { MPV_RENDER_PARAM_INVALID, nullptr }
        };
        mpv_render_context_render(m_obj->m_mpvContext, params);

        m_obj->window()->resetOpenGLState();
    }
};

/***
 * MpvObject
 */
void MpvObject::onUpdateStatic(void *ctx)
{
    auto *self = static_cast<MpvObject *>(ctx);
    emit self->onUpdate();
}

MpvObject::MpvObject(QQuickItem *parent) : QQuickFramebufferObject(parent)
{
    m_mpv = mpv::qt::Handle::fromRawHandle(mpv_create());
    Q_ASSERT_X(m_mpv != nullptr, "MpvObject", "could not create mpv context");

    m_mpv.setProperty(QStringLiteral("terminal"), "yes");

    int err = mpv_initialize(m_mpv);
    Q_ASSERT_X(err >= 0,"MpvObject", "could not initialize mpv context");

    connect(this, &MpvObject::onUpdate, this, &MpvObject::doUpdate, Qt::QueuedConnection);

    mpv_request_log_messages(m_mpv, "info");

    QObject::connect(QCoreApplication::instance(), &QCoreApplication::aboutToQuit, this, &MpvObject::cleanup);

    QTimer::singleShot(0, [=](){
        m_settings = qobject_cast<NSettings *>(nsetings_provider());
        m_playlist = qobject_cast<Playlist *>(playlist_provider());
        m_history = qobject_cast<History *>(history_provider());
        initialize();
    });
}

MpvObject::~MpvObject()
{
    if (m_mpvContext != nullptr) {
        mpv_render_context_free(m_mpvContext);
        m_mpvContext = nullptr;
    }
}

QQuickFramebufferObject::Renderer *MpvObject::createRenderer() const
{
    window()->setPersistentOpenGLContext(true);
    window()->setPersistentSceneGraph(true);
    return new MpvRenderer(const_cast<MpvObject *>(this));
}

void MpvObject::cleanup()
{
    // save current file position to history
    if (m_status > PS_Idle) {
        saveFileHistory();
    }
    m_history->save();
}

void MpvObject::loadFile(const QUrl &file)
{
    QFileInfo fi(file.toLocalFile());
    if (!fi.exists()) {
        setErrorMsg(QStringLiteral("File does not exists, %1").arg(file.toLocalFile()));
        m_playlist->nextFile();
        return;
    }
    if (!fi.isReadable()) {
        setErrorMsg(QStringLiteral("File is not readabale, %1").arg(file.toLocalFile()));
        m_playlist->nextFile();
        return;
    }


    qInfo() << "loading file:" << file;

    // save current file position
    saveFileHistory();

    // start loading new file
    commandLoadFile(file);

    // set new file as recent file
    m_settings->setValue(QStringLiteral("Recent/lastFile"), file.toString());
}

bool MpvObject::tryLoadFromLastFile()
{
    if (m_status != PS_Idle) {
        return false;
    }

    if (m_settings->contains(QStringLiteral("Recent/lastFile"))) {
        QString lastFile = m_settings->value(QStringLiteral("Recent/lastFile"), "").toString();
        QUrl lastUrl(lastFile);
        if (fileExists(lastUrl)) {
            m_playlist->populateFromFile(lastUrl);
            return true;
        }
    }
    return false;
}

void MpvObject::loadUrl(const QUrl &url, int resolutionIndex)
{
    if (url.isValid()) {
        qInfo() << "loading url:" << url;

        // clear playlist as it doesnt support network streaming
        m_playlist->clearPlaylist();

        // m_file has to be set before calling setStreamResolution()
        m_file = url;

        // set correct stream resolution
        setStreamResolution(resolutionIndex);

        // start loading stream
        commandLoadFile(url);
    }
}

void MpvObject::setPause(bool set)
{
    m_mpv.setProperty(QStringLiteral("pause"), set);

    m_status = set ? PS_Paused : PS_Playing;
    emit statusChanged(m_status);

    if (!set) {
        if (!m_delayedPropTimer.isActive()) {
            m_delayedPropTimer.start();
        }
    } else {
        m_delayedPropTimer.stop();
    }
}

void MpvObject::togglePause()
{
    setPause(!m_mpv.property(QStringLiteral("pause")).toBool());
}

void MpvObject::setPlaybackPosition(double absolutePosition)
{
    if (absolutePosition >= 0.0) {
        m_mpv.commandAsync(QStringList({"seek", QString::number(absolutePosition), "absolute"}));
    }
}

// relative seek
void MpvObject::seekPlayback(double val)
{
    if (m_playbackPosition + val < 0.1) {
        setPlaybackPosition(0.1);
    } else {
        m_mpv.commandAsync(QStringList({"seek", QString::number(val)}));
    }
}

bool MpvObject::fileExists(const QUrl &file)
{
    QFileInfo fi(file.toLocalFile());
    return fi.exists() && fi.isFile();
}

void MpvObject::addSubtitleFile(const QUrl &file)
{
    if (!fileExists(file)) {
        return;
    }

    m_mpv.command(QStringList({"sub-add", file.toLocalFile()}));
    loadTrackInfo();
}

// removes current external subtitle
void MpvObject::removeSubtitle()
{
    m_mpv.command("sub-remove");
}

void MpvObject::nextSubtitleTrack()
{
    if (m_subId + 1 >  m_subNames.size()) {
        setSubId(1);
    } else {
        setSubId(m_subId + 1);
    }
}

void MpvObject::nextAudioTrack()
{
    if (m_audioId + 1 >  m_audioNames.size()) {
        setAudioId(1);
    } else {
        setAudioId(m_audioId + 1);
    }
}

bool MpvObject::fileIsVideo(const QUrl &file) const
{
    QFileInfo fileInfo(file.toLocalFile());
    return videoSuffixes().contains(fileInfo.suffix(), Qt::CaseInsensitive);
}

bool MpvObject::fileIsSubtitle(const QUrl &file) const
{
    QFileInfo fileInfo(file.toLocalFile());
    return subtitleSuffixes().contains(fileInfo.suffix(), Qt::CaseInsensitive);
}

QList<QUrl> MpvObject::removeNonVideoFiles(const QList<QUrl> &list)
{
    QList<QUrl> res;
    res.reserve(list.size());
    for (const QUrl &url : qAsConst(list)) {
        QString file = url.toString();
        if (fileIsVideo(file)) {
            res.append(url);
        }
    }
    return res;
}

void MpvObject::updateDelayedProperties()
{
    emit playbackPositionChanged(m_playbackPosition);

    m_playbackPositionNormalized = m_playbackPosition / m_playbackDuration;
    emit playbackPositionNormalizedChanged(m_playbackPositionNormalized);

    m_playbackPositionString = formatTime(m_playbackPosition);
    emit playbackPositionStringChanged(m_playbackPositionString);

    if (m_streamActive) {
        m_cacheSize = m_mpv.property(QStringLiteral("cache-size")).toInt();
        m_cacheFree = m_mpv.property(QStringLiteral("cache-free")).toInt();
        m_cacheUsed = m_mpv.property(QStringLiteral("cache-used")).toInt();
        emit cacheChanged();
    }
}

QString MpvObject::networkStreamingExecutablePath() const
{
#ifdef __linux__
    return QStringLiteral("/usr/bin/youtube-dl");
#else
    return QCoreApplication::applicationDirPath() + "/youtube-dl.exe";
#endif
}

bool MpvObject::networkStreamingSupported() const
{
    return QFile::exists(networkStreamingExecutablePath());
}

void MpvObject::setStreamResolution(NetworkStreamResolution nsr)
{
    if (m_file.isLocalFile()) {
        return;
    }

    m_streamResolution = nsr;
    emit streamResolutionChanged(m_streamResolution);

    QPoint resolution;
    switch (nsr) {
    case NSR_Screen: {
        QScreen *screen = QGuiApplication::primaryScreen();
        QRect screenGeometry = screen->geometry();
        resolution = QPoint(screenGeometry.width(), screenGeometry.height());
        break;
    }
    case NSR_P480: {
        resolution = QPoint(640, 480);
        break;
    }
    case NSR_P720: {
        resolution = QPoint(1280, 720);
        break;
    }
    case NSR_P1080: {
        resolution = QPoint(1920, 1080);
        break;
    }
    case NSR_P1440: {
        resolution = QPoint(2560, 1440);
        break;
    }
    case NSR_P2160: {
        resolution = QPoint(3840, 2160);
        break;
    }
    }

    QString format = QString(QStringLiteral("bestvideo[width<=%1][height<=%2]+bestaudio")).arg(QString::number(resolution.x()), QString::number(resolution.y()));
    m_mpv.setProperty(QStringLiteral("ytdl-format"), format);

    // if we are already playing stream, we have to reload stream to apply new resolution
    if (m_status > PS_Idle) {
        commandLoadFile(m_file);

        if (m_playbackPosition > 0.0) {
            MpvOneShotEvent event;
            event.eventId = MPV_EVENT_FILE_LOADED;
            event.callback = [=]() {
                setPlaybackPosition(m_playbackPosition);
            };
            m_oneShotEvents.append(event);
        }
    }
}

void MpvObject::setStreamResolution(int resolutionIndex)
{
    setStreamResolution(static_cast<NetworkStreamResolution>(resolutionIndex));
}

QString MpvObject::getVideoFilters() const
{
    const QStringList suffixes = videoSuffixes();
    std::stringstream ss;
    ss << "Video files (";
    for (const QString &filter : qAsConst(suffixes)) {
        ss << "*." << filter.toUtf8().data() << " ";
        ss << "*." << filter.toUpper().toUtf8().data() << " ";
    }
    ss << ")";
    return QString::fromStdString(ss.str());
}

QString MpvObject::getSubtitleFilters() const
{
    const QStringList suffixes = subtitleSuffixes();
    std::stringstream ss;
    ss << "Subtitle files (";
    for (const QString &filter : qAsConst(suffixes)) {
        ss << "*." << filter.toUtf8().data() << " ";
        ss << "*." << filter.toUpper().toUtf8().data() << " ";
    }
    ss << ")";
    return QString::fromStdString(ss.str());
}

QString MpvObject::playbackStatusString() const
{
    QString status;
    switch (m_status) {
    case MpvObject::PS_Idle:
        status = QStringLiteral("Idle");
        break;
    case MpvObject::PS_Paused:
        status = QStringLiteral("Paused");
        break;
    case MpvObject::PS_Playing:
        status = QStringLiteral("Playing");
        break;
    }
    return status;
}

QString MpvObject::formatTime(double time) const
{
    return QDateTime::fromSecsSinceEpoch(static_cast<int>(time)).toUTC().toString(QStringLiteral("hh:mm:ss"));
}

void MpvObject::dumpProperty(const QString &property)
{
    if (!property.isEmpty()) {
        qInfo() << property << m_mpv.property(property);
    }
}

void MpvObject::setAudioId(int audioId)
{
    m_audioId = audioId;
    emit audioChanged();

    m_mpv.command(QStringList({"set", "aid", QString::number(audioId)}));
}

void MpvObject::setVolumeLevel(double volumeLevel)
{
    if (volumeLevel > 100.0) {
        volumeLevel = 100.0;
    }
    else if (volumeLevel < 0.0) {
        volumeLevel = 0.0;
    }

    m_volumeLevel = volumeLevel;
    emit volumeLevelChanged(m_volumeLevel);

    double mpvVol = m_volumeMuted ? 0.0 : m_volumeLevel;
    mpv_set_option(m_mpv, "volume", MPV_FORMAT_DOUBLE, &mpvVol);
}

void MpvObject::setVolumeMuted(bool volumeMuted)
{
    m_volumeMuted = volumeMuted;
    emit volumeMutedChanged(m_volumeMuted);

    double mpvVol = m_volumeMuted ? 0.0 : m_volumeLevel;
    mpv_set_option(m_mpv, "volume", MPV_FORMAT_DOUBLE, &mpvVol);
}

void MpvObject::setVolumeNormalized(bool volumeNormalized)
{
    m_volumeNormalized = volumeNormalized;
    emit volumeNormalizedChanged(m_volumeNormalized);

    m_mpv.setProperty(QStringLiteral("af"),
                         m_volumeNormalized ? "lavfi=[alimiter=10:1:1:5:8000]" : "");
}

void MpvObject::setSubId(int subId)
{
    m_subId = subId;
    emit subChanged();

    m_mpv.command(QStringList({"set", "sid", QString::number(subId)}));
}

void MpvObject::setSubVisible(bool subVisible)
{
    m_subVisible = subVisible;
    emit subVisibleChanged(m_subVisible);

    m_mpv.command(QStringList({"set", "sub-visibility", subVisible ? "yes" : "no"}));
}

void MpvObject::setSubOverrideStyle(bool subOverrideStyle)
{
    m_subOverrideStyle = subOverrideStyle;
    emit subOverrideStyleChanged(m_subOverrideStyle);

    m_mpv.setProperty(QStringLiteral("sub-ass-override"),
                         m_subOverrideStyle ? "strip" : "");
}

// connected to onUpdate() signal makes sure it runs on the GUI thread
void MpvObject::doUpdate()
{
    update();
}

void MpvObject::onMpvEvents()
{
    // Process all events, until the event queue is empty
    while (m_mpv != nullptr) {
        mpv_event *event = mpv_wait_event(m_mpv, 0);
        if (event->event_id == MPV_EVENT_NONE) {
            break;
        }
        handleMpvEvent(event);
    }
}

void MpvObject::initialize()
{
    mpv_observe_property(m_mpv, 0, "time-pos", MPV_FORMAT_DOUBLE);
    mpv_observe_property(m_mpv, 0, "sid", MPV_FORMAT_INT64);
    mpv_observe_property(m_mpv, 0, "aid", MPV_FORMAT_INT64);
    mpv_observe_property(m_mpv, 0, "frame-drop-count", MPV_FORMAT_INT64);
    mpv_observe_property(m_mpv, 0, "mistimed-frame-count", MPV_FORMAT_INT64);

    connect(m_playlist, &Playlist::loadFile, this, &MpvObject::loadFile);
    connect(m_settings, &NSettings::hwDecodingChanged, [=] (){
        m_mpv.setProperty(QStringLiteral("hwdec"), m_settings->hwDecoding ? "auto" : "no");
        m_currentHwDec = m_mpv.property(QStringLiteral("hwdec-current")).toString();
        emit currentHwDecChanged(m_currentHwDec);
    });
    connect(m_settings, &NSettings::subAutoLoadChanged, [=] (){
        m_mpv.setProperty(QStringLiteral("sub-auto"),
                             m_settings->subAutoLoad == SAL_None ? "no" : m_settings->subAutoLoad == SAL_Exact ? "exact" : "fuzzy");
    });
    connect(m_settings, &NSettings::mpvMsgLevelChanged, [=] (){
        QString level;
        switch (m_settings->mpvMsgLevel) {
        case MML_None:
            level = QStringLiteral("no");
            break;
        case MML_Fatal:
            level = QStringLiteral("fatal");
            break;
        case MML_Error:
            level = QStringLiteral("error");
            break;
        case MML_Warn:
            level = QStringLiteral("warn");
            break;
        case MML_Info:
            level = QStringLiteral("info");
            break;
        case MML_Status:
            level = QStringLiteral("status");
            break;
        case MML_Verbose:
            level = QStringLiteral("v");
            break;
        case MML_Debug:
            level = QStringLiteral("debug");
            break;
        case MML_Trace:
            level = QStringLiteral("trace");
            break;
        }
        m_mpv.setProperty(QStringLiteral("msg-level"),
                             QStringLiteral("all=%1").arg(level));
    });

    // setup timer for delayed signals
    m_delayedPropTimer.setInterval(500);
    QObject::connect(&m_delayedPropTimer, &QTimer::timeout, this, &MpvObject::updateDelayedProperties);

    // Emit file signal for qml initializarion
    emit fileChanged(m_file);
}

void MpvObject::rendererCreated()
{
    QUrl file = m_settings->parser.file;
    QUrl folder = m_settings->parser.folder;
    QUrl sub = m_settings->parser.subtitle;

    if (!file.isEmpty()) {
        m_playlist->populateFromFile(file);
        if (!sub.isEmpty()) {
            addSubtitleFile(sub);
        }
    } else if (!folder.isEmpty()) {
        m_playlist->populateFromFolder(folder);
    }
}

void MpvObject::handleMpvEvent(mpv_event *event)
{
    for (int i = 0; i < m_oneShotEvents.size(); i++)
    {
        MpvOneShotEvent oneShot = m_oneShotEvents.at(i);
        if (oneShot.eventId == event->event_id) {
            oneShot.callback();
            m_oneShotEvents.removeAt(i);
        }
    }

    switch (event->event_id) {
    case MPV_EVENT_PROPERTY_CHANGE: {
        auto *prop = static_cast<mpv_event_property *>(event->data);
        const QString propName(prop->name);
        if ((propName == QStringLiteral("time-pos")) && prop->format == MPV_FORMAT_DOUBLE) {
            m_playbackPosition = *static_cast<double *>(prop->data);
        } else if ((propName == QStringLiteral("aid")) && prop->format == MPV_FORMAT_INT64) {
            m_audioId = *static_cast<int *>(prop->data);
            emit audioChanged();
        } else if ((propName == QStringLiteral("sid")) && prop->format == MPV_FORMAT_INT64) {
            m_subId = *static_cast<int *>(prop->data);
            emit subChanged();
        } else if ((propName == QStringLiteral("frame-drop-count")) && prop->format == MPV_FORMAT_INT64) {
            m_frameDropCount += *static_cast<int *>(prop->data);
            emit frameCountersChanged();
        } else if ((propName == QStringLiteral("mistimed-frame-count")) && prop->format == MPV_FORMAT_INT64) {
            m_mistimedFrameCount += *static_cast<int *>(prop->data);
            emit frameCountersChanged();
        }
        break;
    }
    case MPV_EVENT_VIDEO_RECONFIG: {
        // Retrieve the new video size.
        int64_t w, h;
        if (mpv_get_property(m_mpv, "dwidth", MPV_FORMAT_INT64, &w) >= 0 &&
                mpv_get_property(m_mpv, "dheight", MPV_FORMAT_INT64, &h) >= 0 &&
                w > 0 && h > 0)
        {
            m_dimensions = QPoint(static_cast<int>(w), static_cast<int>(h));
            emit fileChanged(m_file);
        }
        break;
    }
    case MPV_EVENT_END_FILE: {
        auto *endFile = static_cast<mpv_event_end_file*>(event->data);
        if (endFile->reason == MPV_END_FILE_REASON_EOF) {
            resetProperties();
            m_delayedPropTimer.stop();
            m_playlist->nextFile();
        }
        break;
    }
    case MPV_EVENT_FILE_LOADED: {
        fileLoaded();
        m_delayedPropTimer.start();
        break;
    }
    default: ;
        // Ignore uninteresting or unknown events.
    }
}

void MpvObject::commandLoadFile(const QUrl &url)
{
    m_mpv.command(QStringList({"loadfile", url.toString()}));

    // disable pause
    setPause(false);

    if (m_file != url) {
        m_file = url;
        emit fileChanged(m_file);
    }

    if (!url.isLocalFile()) {
        m_buffering = true;
        emit bufferingChanged(m_buffering);

        m_streamActive = true;
        emit streamActiveChanged(m_streamActive);
    } else {
        if (m_streamActive) {
            m_streamActive = false;
            emit streamActiveChanged(m_streamActive);
        }
    }
}

void MpvObject::saveFileHistory()
{
    if (m_file.isLocalFile()) {
        double pos = (m_playbackPosition / m_playbackDuration > 0.95) ? 0.0 : m_playbackPosition;
        m_history->pushFileToFront(m_file, pos);
    }
}

void MpvObject::fileLoaded()
{
    m_status = PS_Playing;
    emit statusChanged(m_status);

    if (m_buffering) {
        m_buffering = false;
        emit bufferingChanged(m_buffering);
    }

    m_title = m_mpv.property(QStringLiteral("media-title")).toString();
    emit fileChanged(m_file);

    m_currentHwDec = m_mpv.property(QStringLiteral("hwdec-current")).toString();
    emit currentHwDecChanged(m_currentHwDec);

    m_playbackDuration = m_mpv.property(QStringLiteral("duration")).toDouble();
    emit playbackDurationChanged(m_playbackDuration);

    double fileTime = m_history->fileTime(m_file);
    setPlaybackPosition(fileTime);

    updateDelayedProperties();

    saveFileHistory();

    loadTrackInfo();

    // set default sub track
    const QString defaultSubTrack = m_settings->subDefaultTrack;
    if (!defaultSubTrack.isEmpty()) {
        for (int i = 0; i < m_subNames.size(); i++) {
            if (defaultSubTrack == m_subNames[i]) {
                setSubId(i + 1);
                break;
            }
        }
    }

    // set default audio track
    const QString defaultAudioTrack = m_settings->audioDefaultTrack;
    if (!defaultAudioTrack.isEmpty()) {
        for (int i = 0; i < m_audioNames.size(); i++) {
            if (defaultAudioTrack == m_audioNames[i]) {
                setAudioId(i + 1);
                break;
            }
        }
    }
}

void MpvObject::loadTrackInfo()
{
    m_subNames.clear();
    m_audioNames.clear();

    mpv_node node{};
    mpv_get_property(m_mpv, "track-list", MPV_FORMAT_NODE, &node);
    if (node.format == MPV_FORMAT_NODE_ARRAY) {
        mpv_node_list *nodeList = node.u.list;
        for (int i = 0; i < nodeList->num; i++) {
            if (nodeList->values[i].format != MPV_FORMAT_NODE_MAP) {
                continue;
            }

            QString type;
            QString lang;

            mpv_node_list *subNodeList = nodeList->values[i].u.list;
            for (int n = 0; n < subNodeList->num; n++) {
                if (subNodeList->values[n].format == MPV_FORMAT_STRING && QString(subNodeList->keys[n]) == QStringLiteral("type")) {
                    type = subNodeList->values[n].u.string;
                    //qDebug() << "type: " << type;
                } else if (subNodeList->values[n].format == MPV_FORMAT_STRING && QString(subNodeList->keys[n]) == QStringLiteral("lang")) {
                    lang = subNodeList->values[n].u.string;
                    //qDebug() << "lang: " << lang;
                }
            }

            if (lang.size() == 0) {
                lang = QStringLiteral("Missing label");
            }

            if (type == QStringLiteral("sub")) {
                m_subNames.append(lang);
            } else if (type == QStringLiteral("audio")) {
                m_audioNames.append(lang);
            }
        }
    }
    emit subChanged();
    emit audioChanged();
}

void MpvObject::resetProperties()
{
    m_status = PS_Idle;
    emit statusChanged(m_status);

    m_file = QUrl();
    m_title = QString();
    emit fileChanged(m_file);

    m_currentHwDec = QString();
    emit currentHwDecChanged(m_currentHwDec);

    m_playbackDuration = 0.0;
    emit playbackDurationChanged(m_playbackDuration);

    m_playbackPosition = 0.0;
    emit playbackPositionChanged(m_playbackPosition);

    m_playbackPositionNormalized = 0.0;
    emit playbackPositionNormalizedChanged(m_playbackPositionNormalized);

    m_playbackPositionString = QStringLiteral("00:00:00"    );
    emit playbackPositionStringChanged(m_playbackPositionString);

    m_streamActive = false;
    emit streamActiveChanged(m_streamActive);

    m_frameDropCount = 0;
    m_mistimedFrameCount = 0;
    emit frameCountersChanged();

    m_cacheSize = 0;
    m_cacheFree = 0;
    m_cacheUsed = 0;
    emit cacheChanged();
}

void MpvObject::setErrorMsg(const QString &err)
{
    m_errorMsg = err;
    emit errorMsgChanged(m_errorMsg);
}
