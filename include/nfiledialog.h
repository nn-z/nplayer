#include <utility>

#ifndef NFILEDIALOG_H
#define NFILEDIALOG_H

#include <QDir>
#include <QQuickWindow>
#include <QUrl>


class NFileDialog : public QQuickWindow
{
    Q_OBJECT

    Q_PROPERTY(QStringList files READ files NOTIFY filesChanged)
    Q_PROPERTY(QDir path READ path NOTIFY pathChanged)
    Q_PROPERTY(QStringList userPaths READ userPaths NOTIFY userPathsChanged)
    Q_PROPERTY(QStringList systemPaths READ systemPaths NOTIFY systemPathsChanged)
    Q_PROPERTY(FilterType filter READ filter WRITE setFilter NOTIFY filterChanged)
    Q_PROPERTY(bool hidden READ hidden WRITE setHidden NOTIFY hiddenChanged)

public:
    enum FilterType {
        VideoFiles,
        SubtitleFiles,
        Folders
    };
     Q_ENUM(FilterType)

public:
    explicit NFileDialog(QWindow *parent = nullptr);

    QStringList files() const
    {
        return m_files;
    }

    QDir path() const
    {
        return m_path;
    }

    QStringList userPaths() const
    {
        return m_userPaths;
    }

    FilterType filter() const
    {
        return m_filter;
    }

    QStringList systemPaths() const
    {
        return m_systemPaths;
    }

    bool hidden() const
    {
        return m_hidden;
    }

signals:
    void filesChanged(QStringList files);

    void pathChanged(QDir path);

    void userPathsChanged(QStringList userPaths);

    void filterChanged(FilterType filter);

    void systemPathsChanged(QStringList systemPaths);

    void hiddenChanged(bool hidden);

public slots:
    void parentDir();
    void relativeDir(const QString& dir);
    void absoluteDir(const QString& dir);
    QString pathString() const;

    void addUserPath(int index);
    void removeUserPath(int index);
    QString userPathString(int index) const;

    QString systemPathString(int index) const;

    bool isFile(const QString& relativeFile);

    QString itemType(int index) const;

    QUrl convertToUrl(const QString &relativeFile) const;

    void setFilter(FilterType filter);

    void setHidden(bool hidden);

private:
    void processDir();

    QStringList m_files;
    QDir m_path;
    QStringList m_userPaths;
    FilterType m_filter{NFileDialog::VideoFiles};
    QStringList m_systemPaths;
    bool m_hidden{false};
};

#endif // NFILEDIALOG_H
