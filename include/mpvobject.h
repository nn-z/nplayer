#ifndef MPVOBJECT_H
#define MPVOBJECT_H

#include <QMetaType>
#include <QQuickFramebufferObject>
#include <QTimer>
#include <QUrl>

#include <mpv/client.h>
#include <mpv/render_gl.h>

#include "mpvhelper.h"

class MpvRenderer;
class NSettings;
class Playlist;
class History;

class MpvObject : public QQuickFramebufferObject
{
    Q_OBJECT

    // playback
    Q_PROPERTY(double playbackPosition READ playbackPosition NOTIFY playbackPositionChanged)
    Q_PROPERTY(double playbackPositionNormalized READ playbackPositionNormalized NOTIFY playbackPositionNormalizedChanged)
    Q_PROPERTY(QString playbackPositionString READ playbackPositionString NOTIFY playbackPositionStringChanged)
    Q_PROPERTY(double playbackDuration READ playbackDuration NOTIFY playbackDurationChanged)
    Q_PROPERTY(PlaybackStatus status READ status NOTIFY statusChanged)

    // file
    Q_PROPERTY(QUrl file READ file NOTIFY fileChanged)
    Q_PROPERTY(QString title READ title NOTIFY fileChanged)
    Q_PROPERTY(QPoint dimensions READ dimensions NOTIFY fileChanged)

    // video
    Q_PROPERTY(QString currentHwDec READ currentHwDec NOTIFY currentHwDecChanged)

    // audio
    Q_PROPERTY(int audioId READ audioId WRITE setAudioId NOTIFY audioChanged)
    Q_PROPERTY(QStringList audioNames READ audioNames NOTIFY audioChanged)

    // volume
    Q_PROPERTY(double volumeLevel READ volumeLevel WRITE setVolumeLevel NOTIFY volumeLevelChanged)
    Q_PROPERTY(bool volumeMuted READ volumeMuted WRITE setVolumeMuted NOTIFY volumeMutedChanged)
    Q_PROPERTY(bool volumeNormalized READ volumeNormalized WRITE setVolumeNormalized NOTIFY volumeNormalizedChanged)

    // subtitles
    Q_PROPERTY(int subId READ subId WRITE setSubId NOTIFY subChanged)
    Q_PROPERTY(QStringList subNames READ subNames NOTIFY subChanged)
    Q_PROPERTY(bool subVisible READ subVisible WRITE setSubVisible NOTIFY subVisibleChanged)
    Q_PROPERTY(bool subOverrideStyle READ subOverrideStyle WRITE setSubOverrideStyle NOTIFY subOverrideStyleChanged)

    // frame counters
    Q_PROPERTY(int frameDropCount READ frameDropCount NOTIFY frameCountersChanged)
    Q_PROPERTY(int mistimedFrameCount READ mistimedFrameCount NOTIFY frameCountersChanged)

    // network
    Q_PROPERTY(bool buffering READ buffering NOTIFY bufferingChanged)
    Q_PROPERTY(NetworkStreamResolution streamResolution READ streamResolution NOTIFY streamResolutionChanged)
    Q_PROPERTY(bool streamActive READ streamActive NOTIFY streamActiveChanged)

    // cache
    Q_PROPERTY(int cacheSize READ cacheSize NOTIFY cacheChanged)
    Q_PROPERTY(int cacheFree READ cacheFree NOTIFY cacheChanged)
    Q_PROPERTY(int cacheUsed READ cacheUsed NOTIFY cacheChanged)

    Q_PROPERTY(QString errorMsg READ errorMsg NOTIFY errorMsgChanged)

    friend class MpvRenderer;

public:
    enum PlaybackStatus {
        PS_Idle        = 0x01,
        PS_Paused      = 0x02,
        PS_Playing     = 0x04,
    };
    Q_ENUM(PlaybackStatus)

    enum NetworkStreamResolution {
        NSR_Screen,
        NSR_P480,
        NSR_P720,
        NSR_P1080,
        NSR_P1440,
        NSR_P2160
    };
    Q_ENUM(NetworkStreamResolution)

    enum LogType {
        LT_None,
        LT_Console
    };
    Q_ENUM(LogType)

    enum SubAutoLoad {
        SAL_None,
        SAL_Exact,
        SAL_Fuzzy
    };
    Q_ENUM(SubAutoLoad)

    enum MpvMsgLevel {
        MML_None,
        MML_Fatal,
        MML_Error,
        MML_Warn,
        MML_Info,
        MML_Status,
        MML_Verbose,
        MML_Debug,
        MML_Trace
    };
    Q_ENUM(MpvMsgLevel)

public:
    static QStringList videoSuffixes()
    {
        static QStringList suffixes({"mkv", "wmv", "mp4", "avi", "flv", "m4v", "mov"});
        return suffixes;
    }

    static QStringList subtitleSuffixes()
    {
        static QStringList suffixes({"srt", "sub", "ssa"});
        return suffixes;
    }

public:
    static void onUpdateStatic(void *ctx);

    explicit MpvObject(QQuickItem *parent = nullptr);

    ~MpvObject() override;
    Renderer *createRenderer() const override;

    void cleanup();

public:
    double playbackPosition() const
    {
        return m_playbackPosition;
    }

    double playbackDuration() const
    {
        return m_playbackDuration;
    }

    QString errorMsg() const
    {
        return m_errorMsg;
    }

    int mistimedFrameCount() const
    {
        return m_mistimedFrameCount;
    }

    double volumeLevel() const
    {
        return m_volumeLevel;
    }

    bool volumeMuted() const
    {
        return m_volumeMuted;
    }

    bool volumeNormalized() const
    {
        return m_volumeNormalized;
    }

    int frameDropCount() const
    {
        return m_frameDropCount;
    }

    int audioId() const
    {
        return m_audioId;
    }

    QStringList audioNames() const
    {
        return m_audioNames;
    }

    int subId() const
    {
        return m_subId;
    }

    QStringList subNames() const
    {
        return m_subNames;
    }

    bool subVisible() const
    {
        return m_subVisible;
    }

    bool subOverrideStyle() const
    {
        return m_subOverrideStyle;
    }

    PlaybackStatus status() const
    {
        return m_status;
    }

    QUrl file() const
    {
        return m_file;
    }

    QString title() const
    {
        return m_title;
    }

    QPoint dimensions() const
    {
        return m_dimensions;
    }

    QString currentHwDec() const
    {
        return m_currentHwDec;
    }

    bool buffering() const
    {
        return m_buffering;
    }

    NetworkStreamResolution streamResolution() const
    {
        return m_streamResolution;
    }

    bool streamActive() const
    {
        return m_streamActive;
    }

    int cacheSize() const
    {
        return m_cacheSize;
    }

    int cacheFree() const
    {
        return m_cacheFree;
    }

    int cacheUsed() const
    {
        return m_cacheUsed;
    }

    QString playbackPositionString() const
    {
        return m_playbackPositionString;
    }

    double playbackPositionNormalized() const
    {
        return m_playbackPositionNormalized;
    }

public slots:
    void loadFile(const QUrl &file);
    bool tryLoadFromLastFile();
    void loadUrl(const QUrl &url, int resolutionIndex);

    void setPause(bool set);
    void togglePause();

    void setPlaybackPosition(double absolutePosition);
    void seekPlayback(double val);

    bool fileExists(const QUrl &file);

    void addSubtitleFile(const QUrl &file);
    void removeSubtitle();
    void nextSubtitleTrack();

    void nextAudioTrack();

    bool fileIsVideo(const QUrl &file) const;
    bool fileIsSubtitle(const QUrl &file) const;
    QList<QUrl> removeNonVideoFiles(const QList<QUrl> &list);

    void updateDelayedProperties();

    QString networkStreamingExecutablePath() const;
    bool networkStreamingSupported() const;
    void setStreamResolution(NetworkStreamResolution nsr);
    void setStreamResolution(int resolutionIndex);

    QString getVideoFilters() const;
    QString getSubtitleFilters() const;

    QString playbackStatusString() const;

    QString formatTime(double time) const;

    /**
     * @brief keyToAscii, used from qml to convert key to ascii as
     *  event.text is not reliable if Qt.ControlModifier is pressed
     * @param key
     * @return key converted to ascii
     */
    QString keyToAscii(const int key)
    {
        return QString(QChar(key));
    }

    void dumpProperty(const QString &property);

    QString applicationVersion() const
    {
        return QCoreApplication::applicationVersion();
    }

    void setAudioId(int audioId);

    void setVolumeLevel(double volumeLevel);

    void setVolumeMuted(bool volumeMuted);

    void setVolumeNormalized(bool volumeNormalized);

    void setSubId(int subId);

    void setSubVisible(bool subVisible);

    void setSubOverrideStyle(bool subOverrideStyle);

signals:
    void onUpdate();

    void playbackPositionChanged(double playbackPosition);

    void playbackDurationChanged(double playbackDuration);

    void statusChanged(PlaybackStatus status);

    void fileChanged(QUrl file);

    void currentHwDecChanged(QString currentHwDec);

    void audioChanged();

    void volumeLevelChanged(double volumeLevel);

    void volumeMutedChanged(bool volumeMuted);

    void volumeNormalizedChanged(bool volumeNormalized);

    void subChanged();

    void subVisibleChanged(bool subVisible);

    void subOverrideStyleChanged(bool subOverrideStyle);

    void frameCountersChanged();

    void bufferingChanged(bool buffering);

    void streamResolutionChanged(NetworkStreamResolution streamResolution);

    void streamActiveChanged(bool streamActive);

    void cacheChanged();

    void errorMsgChanged(QString errorMsg);

    void playbackPositionStringChanged(QString playbackPositionString);

    void playbackPositionNormalizedChanged(double playbackPositionNormalized);

private slots:
    void doUpdate();
    void onMpvEvents();

private:
    struct MpvOneShotEvent
    {
        std::function<void()> callback;
        mpv_event_id eventId;
    };

    void initialize();

    void rendererCreated();

    void handleMpvEvent(mpv_event *event);

    void commandLoadFile(const QUrl &url);

    void saveFileHistory();

    void fileLoaded();

    void loadTrackInfo();

    void resetProperties();

    void setErrorMsg(const QString &err);

    NSettings *m_settings{nullptr};
    Playlist *m_playlist{nullptr};
    History *m_history{nullptr};

    mpv::qt::Handle m_mpv;
    mpv_render_context *m_mpvContext{nullptr};

    QVector<MpvOneShotEvent> m_oneShotEvents;
    QTimer m_delayedPropTimer;

    QString m_errorMsg;
    int m_mistimedFrameCount{0};
    double m_volumeLevel{100.0};
    bool m_volumeMuted{false};
    bool m_volumeNormalized{false};
    int m_frameDropCount{0};
    int m_audioId{0};
    QStringList m_audioNames;
    int m_subId{0};
    QStringList m_subNames;
    bool m_subVisible{true};
    bool m_subOverrideStyle{false};
    double m_playbackPosition{0.0};
    double m_playbackDuration{0.0};
    PlaybackStatus m_status{PS_Idle};
    QUrl m_file;
    QString m_title;
    QPoint m_dimensions{1280, 720};
    QString m_currentHwDec;
    bool m_buffering{false};
    NetworkStreamResolution m_streamResolution{NSR_Screen};
    bool m_streamActive{false};
    int m_cacheSize{0};
    int m_cacheFree{0};
    int m_cacheUsed{0};
    QString m_playbackPositionString;
    double m_playbackPositionNormalized{0.0};
};

inline QObject *mpvObject_provider(QQmlEngine *engine = nullptr, QJSEngine *scriptEngine = nullptr)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)

    static MpvObject *mpvObject{nullptr};
    if (mpvObject == nullptr) {
        mpvObject = new MpvObject();
    }
    return mpvObject;
}

#endif // MPVOBJECT_H
