#ifndef PLAYLIST_H
#define PLAYLIST_H

#include <QAbstractListModel>
#include <QDir>
#include <QFileSystemWatcher>
#include <QQmlEngine>
#include <QUrl>
#include <QVector>

class NSettings;

class Playlist : public QAbstractListModel
{
    Q_OBJECT

    // index of item we are at currently
    Q_PROPERTY(int activeIndex READ activeIndex WRITE setActiveIndex NOTIFY activeIndexChanged)
    Q_PROPERTY(bool shuffle READ shuffle WRITE setShuffle NOTIFY shuffleChanged)

public:
    enum PlaylistModelRole {
        FileNameRole = Qt::UserRole,
        FilePathRole
    };

public:
    explicit Playlist(QObject *parent = nullptr);

    int rowCount(const QModelIndex &parent) const override;

    QVariant data(const QModelIndex &index, int role) const override;

    QHash<int, QByteArray> roleNames() const override;

    QVector<QUrl> items() const
    {
        return m_items;
    }

    bool setItemAt(int index, const QUrl &item);

    int activeIndex() const
    {
        return m_activeIndex;
    }

    void setActiveIndex(int index);

    bool shuffle() const
    {
        return m_shuffle;
    }

    void setShuffle(bool val);

signals:
    void loadFile(const QUrl &file);

    void activeIndexChanged();

    void shuffleChanged();

public slots:
    /**
     * @brief populates playlist, also erases previous playlist
     * QML
     * @param item as filePath
     */
    void populateFromFile(const QUrl &item);

    void populateFromFolder(const QUrl &item);

    QString fileNameAt(int index) const;
    QString filePathAt(int index) const;

    /**
     * @brief clearPlaylist erases entire playlist
     */
    void clearPlaylist();

    /**
     * @brief nextFile selects new active index and emits loadFile()
     * @param ignoreIncrement ignores active index incrementing
     */
    bool nextFile(bool ignoreIncrement = false);

    bool prevFile();

    /**
     * @brief filesDropped
     * QML
     * @param files
     */
    void filesDropped(const QList<QUrl>& files);

    /**
     * @brief addItem
     * @param file
     * @param setIndex as active index
     */
    void addItem(const QUrl& file, bool setIndex = false);

    /**
     * @brief addItems
     * @param files
     * @param setIndex as active index
     */
    void addItems(const QList<QUrl>& files, bool setIndex = false);

private slots:
    void fileSystemWatchChanged();
    void directoryChanged(const QString &path);

private:
    bool validIndex(int index);

    void preInsertItem();
    void postInsertItem();

    void populate(QDir dir, const QUrl& urlIndex = QUrl(), bool autoLoad = true);

    /**
     * @brief randomIndex
     * @return random valid index
     */
    int randomIndex() const;

    /**
     * @brief indexOfFile
     * @param file as filePath
     * @return index of found file or -1
     */
    int indexOf(const QUrl &file) const;

    /**
     * @brief containsFile
     * @param file as filePath
     * @return true if contains file
     */
    bool containsFile(const QUrl& file);

    void shuffleFile();

    QVector<QUrl> m_items;
    int m_activeIndex{0};

    NSettings *m_settings{nullptr};

    QSharedPointer<QFileSystemWatcher> m_fsWatcher;

    bool m_shuffle{false};

    QStringList m_videoSuffixes;

    QDir m_currentDir;
};

inline QObject *playlist_provider(QQmlEngine *engine = nullptr, QJSEngine *scriptEngine = nullptr)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)

    static Playlist *playlist{nullptr};
    if (playlist == nullptr) {
        playlist = new Playlist();
    }
    return playlist;
}

#endif // PLAYLIST_H
