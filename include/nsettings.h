#ifndef NSETTINGS_H
#define NSETTINGS_H

#include <QPoint>
#include <QSettings>
#include <QSharedPointer>

#include "mpvobject.h"

#define N_PROPERTY(_type, _name) \
    Q_PROPERTY(_type _name MEMBER _name NOTIFY _name##Changed) \
    Q_SIGNALS: \
    void _name##Changed(); \
    public: \
    _type _name = {};

/**
 * @brief The NSettings class
 * On linux config is stored in ~/.config
 * On windows config is stored in system registry
 */
class NSettings : public QSettings
{
    Q_OBJECT

    // float params should be avoided, as they will be saved as QVariant

    N_PROPERTY(bool, playlistPopulateFromSubdirectories)
    N_PROPERTY(bool, playlistRepopulateOnDrag)
    N_PROPERTY(bool, hwDecoding)
    N_PROPERTY(bool, autoVideoResize)
    N_PROPERTY(QString, audioDefaultTrack)
    N_PROPERTY(QString, subDefaultTrack)
    N_PROPERTY(int, onScreenControlsHideTime)
    N_PROPERTY(int, onScreenHintHideTime)
    N_PROPERTY(int, onScreenSeekBarHideTime)
    N_PROPERTY(int, cursorHideTime)
    N_PROPERTY(double, onScreenSeekBarOpacity)
    N_PROPERTY(bool, menuItemShowBothShortcuts)
    N_PROPERTY(bool, historyEnabled)
    N_PROPERTY(int, historyMaxSize)
    N_PROPERTY(double, seekShort)
    N_PROPERTY(double, seekMedium)
    N_PROPERTY(double, seekLong)
    N_PROPERTY(double, volumeStep)
    N_PROPERTY(bool, updatesEnabled)
    N_PROPERTY(bool, updatesUseGit)
    N_PROPERTY(MpvObject::LogType, logType)
    N_PROPERTY(MpvObject::MpvMsgLevel, mpvMsgLevel)
    N_PROPERTY(MpvObject::SubAutoLoad, subAutoLoad)
    N_PROPERTY(bool, uiFontCustom)
    N_PROPERTY(QString, uiFontFamily)
    N_PROPERTY(double, uiFontSize)
    N_PROPERTY(bool, uiNativeFileDialog)
    N_PROPERTY(bool, playlistFileSystemWatch)

    // non persistent properties
    N_PROPERTY(bool, onScreenSeekBar)
    N_PROPERTY(bool, onScreenControls)
    N_PROPERTY(bool, onScreenHint)

public:
    struct Parser {
        QUrl file;
        QUrl folder;
        QUrl subtitle;
        bool update{false};
    };

public:
    explicit NSettings(QObject *parent = nullptr);

    Parser parser;

public slots:
    void save();

    void resetSettings();

private:
    struct AbstractSetting
    {
        virtual ~AbstractSetting() = default;
        virtual void save(QSettings *s) = 0;
    };

    template<typename T>
    struct NSetting : AbstractSetting
    {
        T *value;
        QString key;

        NSetting(T *_member, QString  _key)
            : value(_member)
            , key(std::move(_key))
        {}

        void save(QSettings *s) override
        {
            if (!key.isEmpty()) {
                s->setValue(key, *value);
            }
        }
    };

    template<typename T>
    void add(T *member, T defaultValue, const QString &key)
    {
        auto obj = QSharedPointer<NSetting<T>>(new NSetting<T>(member, key));
        m_abstractSettings.append(obj);
        if (!key.isEmpty()) {
            *member = qvariant_cast<T>(value(key, defaultValue));
        } else {
            *member = defaultValue;
        }
    }

    QVector<QSharedPointer<AbstractSetting>> m_abstractSettings;
};

inline QObject *nsetings_provider(QQmlEngine *engine = nullptr, QJSEngine *scriptEngine = nullptr)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)

    static NSettings *nsettings{nullptr};
    if (nsettings == nullptr) {
        nsettings = new NSettings();
    }
    return nsettings;
}

#endif // NSETTINGS_H
