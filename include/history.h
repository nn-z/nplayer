#ifndef HISTORY_H
#define HISTORY_H

#include <QAbstractListModel>
#include <QDate>
#include <QDir>
#include <QHash>
#include <QQmlEngine>
#include <QUrl>
#include <QVector>

class NSettings;

struct HistoryItem
{
    QUrl url;
    double time = 0.0;
    QDate date;
    bool exists{true};

    bool enabled{true};

    HistoryItem() = default;

    explicit HistoryItem(QUrl _url, double _time = 0.0, QDate _date = QDate::currentDate(), bool _exists = true)
        : url(std::move(_url)), time(_time), date(_date), exists(_exists)
    {}

    inline bool operator==(const HistoryItem &item)
    {
        return item.url == url;
    }
};

class History : public QAbstractListModel
{
    Q_OBJECT

    Q_PROPERTY(bool filterActive READ filterActive NOTIFY filterActiveChanged)

public:
    enum HistoryModelRole {
        FileNameRole = Qt::UserRole,
        FilePathRole,
        FileDateRole,
        FileExists,
        ModelEnabled
    };

public:
    explicit History(QObject *parent = nullptr);

    int rowCount(const QModelIndex &parent) const override;

    QVariant data(const QModelIndex &index, int role) const override;

    QHash<int, QByteArray> roleNames() const override;

    void save();

    /**
     * @brief fileTime
     * @param url
     * @return file time or -1.f
     */
    double fileTime(const QUrl &url) const;

    void pushFileToFront(const QUrl &url, double time);

    int size() const
    {
        return m_items.size();
    }

    bool filterActive() const
    {
        return m_filterActive;
    }

public slots:
    QUrl urlAt(int index) const;
    HistoryItem at(int index) const;

    void clear();

    void filter(const QString &text);
    void clearFilter();

signals:
    void filterActiveChanged(bool filterActive);

private:
    void preInsertItem();
    void postInsertItem();
    void clearItems();

    QString historyFilePath() const;

    void loadHistory();

    QVector<HistoryItem> m_items;

    NSettings *m_settings{nullptr};
    bool m_filterActive{false};
};

inline QObject *history_provider(QQmlEngine *engine = nullptr, QJSEngine *scriptEngine = nullptr)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)

    static History *history{nullptr};
    if (history == nullptr) {
        history = new History();
    }
    return history;
}

#endif // HISTORY_H
