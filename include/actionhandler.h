#ifndef ACTIONHANDLER_H
#define ACTIONHANDLER_H

#include <QMap>
#include <QMapNode>
#include <QMetaType>
#include <QObject>
#include <QQmlEngine>
#include <QSettings>
#include <QSharedPointer>
#include <QVector>

class ActionHandler : public QObject
{
    Q_OBJECT

public:
    enum ActionType {
        Primary,
        Secondary
    };
    Q_ENUM(ActionType)

public:
    explicit ActionHandler(QObject *parent = nullptr);

signals:
    void mapChanged();

public slots:
    void setShortcut(ActionType type, int index, const QString& shortcut);

    QString shortcut(ActionType type, const QString& action);
    QString shortcutByIndex(ActionType type, int index);
    QString actionByIndex(int index);

    int num() const;

    bool containsShortcut(const QString& shortcut);

    void save();

    void reset();

private:
    struct Action
    {
        QString action;
        QStringList shortcuts;

        inline bool operator== (const Action &other)
        {
            return action == other.action;
        }
    };

    void load(const QString& action, const QStringList& defaultShortcuts);

    QSharedPointer<QSettings> m_settings;

    QVector<Action> m_actions;

    QStringList m_cachedShortcuts;
};

inline QObject *actionHandler_provider(QQmlEngine *engine = nullptr, QJSEngine *scriptEngine = nullptr)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)

    static ActionHandler *actionHandler{nullptr};
    if (actionHandler == nullptr) {
        actionHandler = new ActionHandler();
    }
    return actionHandler;
}

#endif // ACTIONHANDLER_H
