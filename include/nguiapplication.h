#ifndef NGUIAPPLICATION_H
#define NGUIAPPLICATION_H

#include <QGuiApplication>
#include <QSharedMemory>
#include <QSharedPointer>

class NGuiApplication : public QGuiApplication
{
public:
    explicit NGuiApplication(int &argc, char **argv);
    ~NGuiApplication() override;

    bool lock() const;

private:
    QSharedPointer<QSharedMemory> m_instance;
};

#endif // NGUIAPPLICATION_H
