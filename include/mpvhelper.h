﻿#ifndef MPVHELPER_H
#define MPVHELPER_H

#include <QHash>
#include <QList>
#include <QMetaType>
#include <QSharedPointer>
#include <QString>
#include <QVariant>

#include <cstring>

#include <mpv/client.h>

namespace mpv {
namespace qt {

struct NodeBuilder {
    explicit NodeBuilder(const QVariant& v)
    {
        set(&m_node, v);
    }

    ~NodeBuilder()
    {
        freeNode(&m_node);
    }

    mpv_node *node()
    {
        return &m_node;
    }

private:
    Q_DISABLE_COPY(NodeBuilder)
    mpv_node m_node{};

    mpv_node_list *createList(mpv_node *dst, bool is_map, int num)
    {
        dst->format = is_map ? MPV_FORMAT_NODE_MAP : MPV_FORMAT_NODE_ARRAY;
        auto *list = new mpv_node_list();
        dst->u.list = list;
        if (list == nullptr) {
            goto err;
        }
        list->values = new mpv_node[num]();
        if (list->values == nullptr) {
            goto err;
        }
        if (is_map) {
            list->keys = new char*[num]();
            if (list->keys == nullptr) {
                goto err;
            }
        }
        return list;
err:
        freeNode(dst);
        return nullptr;
    }

    char *dupQstring(const QString &s)
    {
        QByteArray b = s.toUtf8();
        size_t size = b.size() + 1;
        auto *r = new char[size];
        std::memcpy(r, b.data(), size);
        return r;
    }

    bool testType(const QVariant &v, QMetaType::Type t)
    {
        return static_cast<int>(v.type()) == static_cast<int>(t);
    }

    void set(mpv_node *dst, const QVariant &src) {
        if (testType(src, QMetaType::QString)) {
            dst->format = MPV_FORMAT_STRING;
            dst->u.string = dupQstring(src.toString());
            if (dst->u.string == nullptr) {
                goto fail;
            }
        } else if (testType(src, QMetaType::Bool)) {
            dst->format = MPV_FORMAT_FLAG;
            dst->u.flag = src.toBool() ? 1 : 0;
        } else if (testType(src, QMetaType::Int) ||
                   testType(src, QMetaType::LongLong) ||
                   testType(src, QMetaType::UInt) ||
                   testType(src, QMetaType::ULongLong))
        {
            dst->format = MPV_FORMAT_INT64;
            dst->u.int64 = src.toLongLong();
        } else if (testType(src, QMetaType::Double)) {
            dst->format = MPV_FORMAT_DOUBLE;
            dst->u.double_ = src.toDouble();
        } else if (src.canConvert<QVariantList>()) {
            QVariantList qlist = src.toList();
            mpv_node_list *list = createList(dst, false, qlist.size());
            if (list == nullptr) {
                goto fail;
            }
            list->num = qlist.size();
            for (int n = 0; n < qlist.size(); n++) {
                set(&list->values[n], qlist[n]);
            }
        } else if (src.canConvert<QVariantMap>()) {
            QVariantMap qmap = src.toMap();
            mpv_node_list *list = createList(dst, true, qmap.size());
            if (list == nullptr) {
                goto fail;
            }
            list->num = qmap.size();
            for (int n = 0; n < qmap.size(); n++) {
                list->keys[n] = dupQstring(qmap.keys()[n]);
                if (list->keys[n] == nullptr) {
                    freeNode(dst);
                    goto fail;
                }
                set(&list->values[n], qmap.values()[n]);
            }
        } else {
            goto fail;
        }
        return;
fail:
        dst->format = MPV_FORMAT_NONE;
    }

    void freeNode(mpv_node *dst)
    {
        switch (dst->format) {
        case MPV_FORMAT_STRING:
            delete[] dst->u.string;
            break;
        case MPV_FORMAT_NODE_ARRAY:
        case MPV_FORMAT_NODE_MAP: {
            mpv_node_list *list = dst->u.list;
            if (list != nullptr) {
                for (int n = 0; n < list->num; n++) {
                    if (list->keys != nullptr) {
                        delete[] list->keys[n];
                    }
                    if (list->values != nullptr) {
                        freeNode(&list->values[n]);
                    }
                }
                delete[] list->keys;
                delete[] list->values;
            }
            delete list;
            break;
        }
        default: ;
        }
        dst->format = MPV_FORMAT_NONE;
    }
};

class Handle
{
    struct Container {
        explicit Container(mpv_handle *h) : mpv(h) {}
        Container(const Container&) = delete;
        Container& operator=(const Container&) = delete;
        ~Container() { mpv_terminate_destroy(mpv); }
        mpv_handle *mpv;
    };
    QSharedPointer<Container> sptr;

public:
    static Handle fromRawHandle(mpv_handle *handle)
    {
        Handle h;
        h.sptr = QSharedPointer<Container>(new Container(handle));
        return h;
    }

    // Return the raw handle; for use with the libmpv C API.
    operator mpv_handle*() const { return sptr != nullptr ? (*sptr).mpv : nullptr; }

    inline QVariant property(const QString &name)
    {
        mpv_node node{};
        int err = mpv_get_property(sptr->mpv, name.toUtf8().data(), MPV_FORMAT_NODE, &node);
        if (err < 0) {
            return QVariant();
        }
        QVariant variant = nodeToVariant(&node);
        mpv_free_node_contents(&node);
        return variant;
    }

    inline int setProperty(const QString &name, const QVariant &v)
    {
        NodeBuilder node(v);
        return mpv_set_property(sptr->mpv, name.toUtf8().data(), MPV_FORMAT_NODE, node.node());
    }

    inline QVariant command(const QVariant &args)
    {
        NodeBuilder node(args);
        mpv_node res{};
        int err = mpv_command_node(sptr->mpv, node.node(), &res);
        if (err < 0) {
            return QVariant();
        }
        QVariant variant = nodeToVariant(&res);
        mpv_free_node_contents(&res);
        return variant;
    }

    inline int commandAsync(const QVariant &args)
    {
        NodeBuilder node(args);
        return mpv_command_node_async(sptr->mpv, 1, node.node());
    }

private:
    inline QVariant nodeToVariant(const mpv_node *node)
    {
        switch (node->format) {
        case MPV_FORMAT_STRING:
            return QVariant(QString::fromUtf8(node->u.string));
        case MPV_FORMAT_FLAG:
            return QVariant(static_cast<bool>(node->u.flag));
        case MPV_FORMAT_INT64:
            return QVariant(static_cast<qlonglong>(node->u.int64));
        case MPV_FORMAT_DOUBLE:
            return QVariant(node->u.double_);
        case MPV_FORMAT_NODE_ARRAY: {
            mpv_node_list *list = node->u.list;
            QVariantList qlist;
            qlist.reserve(list->num);
            for (int n = 0; n < list->num; n++) {
                qlist.append(nodeToVariant(&list->values[n]));
            }
            return QVariant(qlist);
        }
        case MPV_FORMAT_NODE_MAP: {
            mpv_node_list *list = node->u.list;
            QVariantMap qmap;
            for (int n = 0; n < list->num; n++) {
                qmap.insert(QString::fromUtf8(list->keys[n]), nodeToVariant(&list->values[n]));
            }
            return QVariant(qmap);
        }
        default: // MPV_FORMAT_NONE, unknown values (e.g. future extensions)
            return QVariant();
        }
    }
};

} // namespace qt
} // namespace mpv

#endif // MPVHELPER_H
