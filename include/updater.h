#ifndef UPDATER_H
#define UPDATER_H

#include <QMap>
#include <QObject>
#include <QQmlEngine>
#include <QUrl>
#include <QtNetwork>

class NSettings;

/**
 * @brief Updater class for windows
 */
class Updater : public QObject
{
    using Md5QUrl = QUrl;
    using FileQUrl = QUrl;

    Q_OBJECT

    Q_PROPERTY(bool updateAvailable MEMBER m_updateAvailable NOTIFY updateAvailableChanged)
    Q_PROPERTY(bool updateRunning MEMBER m_updateRunning NOTIFY updateRunningChanged)
    Q_PROPERTY(QString updateErrorMsg MEMBER m_updateErrorMsg NOTIFY updateErrorMsgChanged)

    enum FileUrl
    {
        Exe,
        LibMpv
    };

public:
    explicit Updater(QObject *parent = nullptr);

signals:
    void updateAvailableChanged();
    void updateRunningChanged();
    void updateErrorMsgChanged();

public slots:
    void checkForUpdate(bool forceCheck = false);
    void doUpdate();

private slots:
    void downloadFinished(QNetworkReply *reply);

private:
    void downloadFile(const QUrl &url);

    bool isHttpRedirect(QNetworkReply *reply) const;

    QString saveFileName(const QUrl &url);

    void saveToDisk(const QString &filename, const QByteArray &data);

    bool compareUrl(const QUrl &a, const QUrl &b) const;

    QString appChecksum() const;
    QString libMpvChecksum()const;

    QString splitChecksum(const QByteArray &data) const;

    QString libMpvPath()const;

    void updateFile(const QString &file, const QString &newFile);

    void deleteUpdateFiles();

    NSettings *m_settings{nullptr};

    QNetworkAccessManager m_manager;
    QVector<QNetworkReply *> m_currentDownloads;

    bool m_updateAvailable{false};
    bool m_updateRunning{false};
    QString m_updateErrorMsg;

    QMap<FileUrl, QPair<Md5QUrl, FileQUrl>> m_urlMap;
};

inline QObject *updater_provider(QQmlEngine *engine = nullptr, QJSEngine *scriptEngine = nullptr)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)

    static Updater *updater{nullptr};
    if (updater == nullptr) {
        updater = new Updater();
    }
    return updater;
}

#endif // UPDATER_H
