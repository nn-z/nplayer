QT += quick
CONFIG += c++17

lessThan(QT_MAJOR_VERSION, 5): error("requires Qt 5.10 or higher")
equals(QT_MAJOR_VERSION, 5):lessThan(QT_MINOR_VERSION, 10): error("requires Qt 5.10 or higher")

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

unix {
    target.path = /usr/bin

    CONFIG += link_pkgconfig
    PKGCONFIG += mpv
}

win32 {
    QT += winextras

    RC_FILE = etc/nplayer.rc

    contains(QMAKE_TARGET.arch, x86_64) {
        LIBS += -L$$PWD/windows/mpv/lib64/ -llibmpv.dll
        mpv.files = windows/mpv/lib64/mpv-1.dll
        openssl.files = windows/openssl/lib64/*
    } else {
        LIBS += -L$$PWD/windows/mpv/lib/ -llibmpv.dll
        mpv.files = windows/mpv/lib/mpv-1.dll
        openssl.files = windows/openssl/lib/*
    }
    INCLUDEPATH += $$PWD/windows/mpv/include
    DEPENDPATH += $$PWD/windows/mpv/include

    SOURCES += \
        src/updater.cpp

    HEADERS += \
        include/updater.h

    ytdl.files = windows/ytdl/*

    CONFIG(debug, debug|release) {
        mpv.path = $$OUT_PWD/debug
        openssl.path = $$OUT_PWD/debug
        ytdl.path = $$OUT_PWD/debug
    } else {
        mpv.path = $$OUT_PWD/release
        openssl.path = $$OUT_PWD/release
        ytdl.path = $$OUT_PWD/release
    }

    INSTALLS += mpv openssl ytdl
}

SOURCES += \
    src/main.cpp \
    src/playlist.cpp \
    src/mpvobject.cpp \
    src/actionhandler.cpp \
    src/history.cpp \
    src/nsettings.cpp \
    src/nfiledialog.cpp \
    src/nguiapplication.cpp

RESOURCES += ui/ui.qrc

# Default rules for deployment.
!isEmpty(target.path): INSTALLS += target

INCLUDEPATH += $$PWD/include

HEADERS += \
    include/playlist.h \
    include/mpvobject.h \
    include/actionhandler.h \
    include/nsettings.h \
    include/mpvhelper.h \
    include/history.h \
    include/nfiledialog.h \
    include/nguiapplication.h
